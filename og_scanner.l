%option c++ prefix="og_scanner_" outfile="og_scanner.cpp"
%option stack noyywrap yylineno 8bit
%{
// make relevant includes before including the parser's tab file
#include <string>
#include <cdk/ast/sequence_node.h>
#include <cdk/ast/expression_node.h>
#include <cdk/ast/lvalue_node.h>
#include "og_parser.tab.h"

// don't change this
#define yyerror LexerError
%}
%x X_STRING
%x X_COMMENT
%x X_ESCAPE
%x X_IGNORE
%%

 /* COMMENTS */
"//".*                                ; /* ignore comments */
"/*"                                  yy_push_state(X_COMMENT);
<X_COMMENT>.|\n                       ; /* ignore multi line comments */
<X_COMMENT>"/*"                       yy_push_state(X_COMMENT);
<X_COMMENT>"*/"                       yy_pop_state();

 /* COMPARATORS */
">="                                  return tGE;
"<="                                  return tLE;
"=="                                  return tEQ;
"!="                                  return tNE;

 /* TYPES */
"auto"                                return tAUTO;
"int"                                 return tINTEGER_TYPE;
"real"                                return tREAL_TYPE;
"string"                              return tSTRING_TYPE;
"ptr"                                 return tPTR;
"public"                              return tPUBLIC;
"require"                             return tREQUIRE;
"nullptr"                             return tNULLPTR;
"procedure"                           return tPROCEDURE;

 /* INSTRUCTIONS */
"input"                               return tINPUT;
"break"                               return tBREAK;
"continue"                            return tCONTINUE;
"return"                              return tRETURN;
"if"                                  return tIF;
"then"                                return tTHEN;
"elif"                                return tELIF;
"else"                                return tELSE;
"for"                                 return tFOR;
"do"                                  return tDO;
"write"                               return tWRITE;
"writeln"                             return tWRITELN;

 /* FUNCTION */
"sizeof"                              return tSIZEOF;

 /* VAR/FUNCTION NAME */
[A-Za-z][A-Za-z0-9_]*                 yylval.s = new std::string(yytext); return tIDENTIFIER;

 /* STRINGS */
\"                                    yy_push_state(X_STRING); yylval.s = new std::string("");
<X_STRING>\"                          yy_pop_state(); return tSTRING;
<X_STRING>\\                          yy_push_state(X_ESCAPE);
<X_STRING>.                           *yylval.s += yytext;

<X_ESCAPE>\\                          *yylval.s += "\\"; yy_pop_state();
<X_ESCAPE>\"                          *yylval.s += "\""; yy_pop_state(); /* escape quote */
<X_ESCAPE>"n"                         *yylval.s += "\n"; yy_pop_state();
<X_ESCAPE>"r"                         *yylval.s += "\r"; yy_pop_state();
<X_ESCAPE>"t"                         *yylval.s += "\t"; yy_pop_state();
<X_ESCAPE>0|00                        *yylval.s += "\0"; yy_pop_state(); yy_push_state(X_IGNORE);
<X_ESCAPE>[0-9a-fA-F]{1,2}            *yylval.s += std::stoi(yytext, nullptr, 16); yy_pop_state();
<X_ESCAPE>.                           *yylval.s += yytext; yy_pop_state();

<X_IGNORE>\\\"                        ;
<X_IGNORE>\"                          yyless(yyleng-1); yy_pop_state();
<X_IGNORE>.                           ;

 /* NUMBER LITERALS */
[0-9]+                                                                   try { yylval.i = std::stoi(yytext); return tINTEGER; } catch (std::out_of_range&) { yyerror("Overflow"); };
0x[0-9a-fA-F]+                                                           try { yylval.i = std::stoi(yytext, nullptr, 16); return tINTEGER; }catch(std::out_of_range&) { yyerror("Overflow"); };
(([0-9]*[.]?[0-9]+)|([0-9]+\.))([eE][+-]?[0-9]+)?  try { yylval.d = std::stod(yytext); return tREAL; } catch (std::out_of_range&) { yyerror("Overflow"); };

 /* OPERATORS */
[-()<>=+*/%;{}.@?,\[\]~]              return *yytext;
"||"                                  return tOR;
"&&"                                  return tAND;

 /* WHITESPACE */
[ \r\t\n]+                              ; /* ignore whitespace */

.                                     yyerror("Unknown character");

%%
