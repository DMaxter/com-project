#include "targets/postfix_writer.h"
#include "ast/all.h" // all.h is automatically generated
#include "og_parser.tab.h"
#include "targets/local_size_calc.h"
#include "targets/type_checker.h"
#include <cdk/ast/expression_node.h>
#include <cdk/ast/integer_node.h>
#include <cdk/types/typename_type.h>
#include <cdk/types/types.h>
#include <sstream>
#include <string>

//---------------------------------------------------------------------------

void og::postfix_writer::do_nil_node(cdk::nil_node *const node, int lvl) {
  // EMPTY
}
void og::postfix_writer::do_data_node(cdk::data_node *const node, int lvl) {
  // EMPTY
}
void og::postfix_writer::do_double_node(cdk::double_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  if (_inFunctionBody) {
    _pf.DOUBLE(node->value());
  } else {
    _pf.SDOUBLE(node->value());
  }
}

void og::postfix_writer::do_not_node(cdk::not_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  node->argument()->accept(this, lvl);
  _pf.INT(0);
  _pf.EQ();
}
void og::postfix_writer::do_and_node(cdk::and_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  int andend = ++_lbl;
  node->left()->accept(this, lvl);
  // only checks right if left is true
  _pf.DUP32();
  _pf.JZ(mklbl(andend));
  node->right()->accept(this, lvl);
  _pf.AND();
  _pf.ALIGN();
  _pf.LABEL(mklbl(andend));
}
void og::postfix_writer::do_or_node(cdk::or_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  int orend = ++_lbl;
  node->left()->accept(this, lvl);
  // only checks right if left is false
  _pf.JNZ(mklbl(orend));
  node->right()->accept(this, lvl);
  _pf.OR();
  _pf.ALIGN();
  _pf.LABEL(mklbl(orend));
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_sequence_node(cdk::sequence_node *const node,
                                          int lvl) {
  for (size_t i = 0; i < node->size(); i++) {
    node->node(i)->accept(this, lvl);
  }
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_integer_node(cdk::integer_node *const node,
                                         int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  if (_inFunctionBody) {
    _pf.INT(node->value());
  } else {
    _pf.SINT(node->value());
  }
}

void og::postfix_writer::do_string_node(cdk::string_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  int literal;

  // Store constant string
  _pf.RODATA();
  _pf.ALIGN();
  _pf.LABEL(mklbl(literal = ++_lbl));
  _pf.SSTRING(node->value());

  if (_inFunctionBody) {
    // Local variable
    _pf.TEXT();
    _pf.ADDR(mklbl(literal));
  } else {
    // Global variable
    _pf.DATA();
    _pf.SADDR(mklbl(literal));
  }
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_neg_node(cdk::neg_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  node->argument()->accept(this, lvl); // determine the value
  if (node->argument()->is_typed(cdk::TYPE_DOUBLE))
    _pf.DNEG();
  else
    _pf.NEG(); // 2-complement
}

//---------------------------------------------------------------------------

bool og::postfix_writer::processBinaryExpression(
    cdk::binary_operation_node *const node, int lvl) {
  bool doubleType;

  if (node->left()->is_typed(cdk::TYPE_DOUBLE) ||
      node->right()->is_typed(cdk::TYPE_DOUBLE)) {
    doubleType = true;
  } else {
    doubleType = false;
  }

  node->left()->accept(this, lvl);
  if (node->left()->is_typed(cdk::TYPE_INT) &&
      node->right()->is_typed(cdk::TYPE_DOUBLE)) {
    _pf.I2D();
  }

  node->right()->accept(this, lvl);
  if (node->left()->is_typed(cdk::TYPE_DOUBLE) &&
      node->right()->is_typed(cdk::TYPE_INT)) {
    _pf.I2D();
  }

  return doubleType;
}

void og::postfix_writer::processPointerArithmetics(
    cdk::binary_operation_node *const node, int lvl, bool addition) {
  if (node->left()->is_typed(cdk::TYPE_INT)) {
    if (addition) {
      node->right()->accept(this, lvl);
      node->left()->accept(this, lvl);
      _pf.INT(cdk::reference_type_cast(node->right()->type())
                  ->referenced()
                  ->size());
      _pf.MUL();
    } else {
      std::cerr << "cannot subtract an address from an int" << std::endl;
    }

  } else if (node->right()->is_typed(cdk::TYPE_INT)) {
    node->left()->accept(this, lvl);
    node->right()->accept(this, lvl);
    _pf.INT(
        cdk::reference_type_cast(node->left()->type())->referenced()->size());
    _pf.MUL();
  }
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_add_node(cdk::add_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = false;

  if (node->is_typed(cdk::TYPE_POINTER)) {
    processPointerArithmetics(node, lvl, true);
    if (node->left()->is_typed(cdk::TYPE_POINTER) &&
        node->right()->is_typed(cdk::TYPE_POINTER)) {
      std::cerr << "cannot add 2 pointers" << std::endl;
    }
  } else {
    doubleType = processBinaryExpression(node, lvl);
  }

  if (doubleType) {
    _pf.DADD();
  } else {
    _pf.ADD();
  }
}
void og::postfix_writer::do_sub_node(cdk::sub_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = false;

  if (node->is_typed(cdk::TYPE_POINTER)) {
    processPointerArithmetics(node, lvl, false);
    if (node->left()->is_typed(cdk::TYPE_POINTER) ||
        node->right()->is_typed(cdk::TYPE_POINTER)) {
      std::shared_ptr<cdk::reference_type> left_type =
          cdk::reference_type_cast(node->left()->type());
      std::shared_ptr<cdk::reference_type> right_type =
          cdk::reference_type_cast(node->right()->type());
      if (left_type->referenced() == right_type->referenced()) {
        node->left()->accept(this, lvl);
        node->right()->accept(this, lvl);
      } else {
        std::cerr << "cannot subtract pointers for different types"
                  << std::endl;
      }
    }
  } else {
    doubleType = processBinaryExpression(node, lvl);
  }

  if (doubleType) {
    _pf.DSUB();
  } else {
    _pf.SUB();
  }
}
void og::postfix_writer::do_mul_node(cdk::mul_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = processBinaryExpression(node, lvl);

  if (doubleType) {
    _pf.DMUL();
    return;
  }

  _pf.MUL();
}
void og::postfix_writer::do_div_node(cdk::div_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = processBinaryExpression(node, lvl);

  if (doubleType) {
    _pf.DDIV();
    return;
  }

  _pf.DIV();
}
void og::postfix_writer::do_mod_node(cdk::mod_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
  _pf.MOD();
}

void og::postfix_writer::do_lt_node(cdk::lt_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = processBinaryExpression(node, lvl);

  if (doubleType) {
    _pf.DCMP();
    _pf.INT(0);
  }

  _pf.LT();
}
void og::postfix_writer::do_le_node(cdk::le_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = processBinaryExpression(node, lvl);

  if (doubleType) {
    _pf.DCMP();
    _pf.INT(0);
  }

  _pf.LE();
}
void og::postfix_writer::do_ge_node(cdk::ge_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = processBinaryExpression(node, lvl);

  if (doubleType) {
    _pf.DCMP();
    _pf.INT(0);
  }

  _pf.GE();
}
void og::postfix_writer::do_gt_node(cdk::gt_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = processBinaryExpression(node, lvl);

  if (doubleType) {
    _pf.DCMP();
    _pf.INT(0);
  }

  _pf.GT();
}
void og::postfix_writer::do_ne_node(cdk::ne_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = processBinaryExpression(node, lvl);

  if (doubleType) {
    _pf.DCMP();
    _pf.INT(0);
  }

  _pf.NE();
}

void og::postfix_writer::do_eq_node(cdk::eq_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = processBinaryExpression(node, lvl);

  if (doubleType) {
    _pf.DCMP();
    _pf.INT(0);
  }

  _pf.EQ();
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_variable_node(cdk::variable_node *const node,
                                          int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  auto symbol = _symtab.find(node->name());

  if (symbol->is_global_variable()) {
    _pf.ADDR(node->name());
  } else {
    _pf.LOCAL(symbol->offset());
  }

  reset_new_symbol();
}

void og::postfix_writer::do_rvalue_node(cdk::rvalue_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  if (node->is_typed(cdk::TYPE_STRUCT)) {
    if (_indexing) {
      node->lvalue()->accept(this, lvl);
      return;
    }

    auto type = cdk::structured_type_cast(node->type());

    size_t offset = 0;
    for (size_t i = 0; i < type->length(); i++) {
      node->lvalue()->accept(this, lvl);
      auto elem = type->component(i);

      if (elem->name() == cdk::TYPE_INT || elem->name() == cdk::TYPE_POINTER ||
          elem->name() == cdk::TYPE_STRING) {
        _pf.INT(offset);
        _pf.ADD();
        _pf.LDINT();
      } else if (elem->name() == cdk::TYPE_DOUBLE) {
        _pf.INT(offset);
        _pf.ADD();
        _pf.LDDOUBLE();
      } else {
        std::cerr << "invalid type in tuple to be returned" << std::endl;
      }

      offset += type->component(i)->size();
    }
  } else {
    node->lvalue()->accept(this, lvl);
    if (node->is_typed(cdk::TYPE_INT) || node->is_typed(cdk::TYPE_POINTER) ||
        node->is_typed(cdk::TYPE_STRING)) {
      _pf.LDINT();
    } else if (node->is_typed(cdk::TYPE_DOUBLE)) {
      _pf.LDDOUBLE();
    } else {
      std::cerr << "invalid type in right value" << std::endl;
    }
  }
}

void og::postfix_writer::do_assignment_node(cdk::assignment_node *const node,
                                            int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool doubleType = false;

  _assignment++;
  node->rvalue()->accept(this, lvl);

  if (node->is_typed(cdk::TYPE_DOUBLE)) {
    doubleType = true;
  }

  if (doubleType) {
    // Check if assignment is (double) = (int)
    if (node->rvalue()->is_typed(cdk::TYPE_INT)) {
      _pf.I2D();
    }

    _pf.DUP64();
  } else {
    _pf.DUP32();
  }

  node->lvalue()->accept(this, lvl);
  if (doubleType) {
    _pf.STDOUBLE();
  } else {
    _pf.STINT();
  }
  _assignment--;

  if (!(_assignment || _evalNode)) {
    if (doubleType) {
      _pf.TRASH(8);
    } else {
      _pf.TRASH(4);
    }
  }
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_evaluation_node(og::evaluation_node *const node,
                                            int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  _evalNode = true;
  node->argument()->accept(this, lvl);
  _evalNode = false;
  if (node->argument()->is_typed(cdk::TYPE_INT) ||
      node->argument()->is_typed(cdk::TYPE_STRING) ||
      node->argument()->is_typed(cdk::TYPE_POINTER)) {
    _pf.TRASH(4);
  } else if (node->argument()->is_typed(cdk::TYPE_DOUBLE)) {
    _pf.TRASH(8);
  } else if (node->argument()->is_typed(cdk::TYPE_STRUCT)) {
    _pf.TRASH(node->argument()->type()->size());
  }

  // Void doesn't need trash
}

void og::postfix_writer::do_write_node(og::write_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  og::tuple_node *tuple = dynamic_cast<og::tuple_node *>(node->argument());

  for (size_t i = 0; i < tuple->length(); i++) {
    cdk::expression_node *elem =
        dynamic_cast<cdk::expression_node *>(tuple->elem(i));

    elem->accept(this, lvl);
    if (elem->is_typed(cdk::TYPE_INT)) {
      _to_declare.insert("printi");
      _pf.CALL("printi");
    } else if (elem->is_typed(cdk::TYPE_STRING)) {
      _to_declare.insert("prints");
      _pf.CALL("prints");
    } else if (elem->is_typed(cdk::TYPE_DOUBLE)) {
      _to_declare.insert("printd");
      _pf.CALL("printd");
    } else {
      std::cerr << "invalid type in write instruction" << std::endl;
    }
    _pf.TRASH(elem->type()->size());
  }

  if (node->changeLine()) {
    _to_declare.insert("println");
    _pf.CALL("println");
  }
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_input_node(og::input_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  if (node->is_typed(cdk::TYPE_INT)) {
    _to_declare.insert("readi");
    _pf.CALL("readi");
    _pf.LDFVAL32();
  } else if (node->is_typed(cdk::TYPE_DOUBLE)) {
    _to_declare.insert("readd");
    _pf.CALL("readd");
    _pf.LDFVAL64();
  }
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_for_node(og::for_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  int forcond, forincr, forend;
  _symtab.push();
  node->initialize()->accept(this, lvl);
  _pf.LABEL(mklbl(forcond = ++_lbl));
  node->condition()->accept(this, lvl);
  _pf.JZ(mklbl(forend = ++_lbl));
  _forIncrement.push(forincr = ++_lbl);
  _forEnd.push(forend);
  node->instruction()->accept(this, lvl);
  _pf.JMP(mklbl(forincr));
  _pf.LABEL(mklbl(forincr));
  node->increment()->accept(this, lvl);
  _pf.JMP(mklbl(forcond));
  _pf.LABEL(mklbl(forend));
  _forIncrement.pop();
  _forEnd.pop();
  _symtab.pop();
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_if_node(og::if_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  int ifend;
  node->condition()->accept(this, lvl);
  _pf.JZ(mklbl(ifend = ++_lbl));
  node->block()->accept(this, lvl + 2);
  _pf.LABEL(mklbl(ifend));
}

//---------------------------------------------------------------------------

void og::postfix_writer::do_if_else_node(og::if_else_node *const node,
                                         int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  int elseblock, ifend;
  node->condition()->accept(this, lvl);
  _pf.JZ(mklbl(elseblock = ++_lbl));
  node->thenblock()->accept(this, lvl + 2);
  _pf.JMP(mklbl(ifend = ++_lbl));
  _pf.LABEL(mklbl(elseblock));
  node->elseblock()->accept(this, lvl + 2);
  _pf.LABEL(mklbl(ifend));
}

void og::postfix_writer::do_function_decl_node(
    og::function_decl_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  std::shared_ptr<og::symbol> function = new_symbol();

  _inFunctionArgs = true;
  _currentFunc = node->identifier();
  _offset = 8;
  if (node->arguments()) {
    _symtab.push();
    node->arguments()->accept(this, lvl);
    _symtab.pop();
  }
  _offset = 0;
  _currentFunc.clear();
  _inFunctionArgs = false;

  _to_declare.insert(function->name());
  reset_new_symbol();
}

void og::postfix_writer::do_function_def_node(og::function_def_node *const node,
                                              int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  _function = new_symbol();
  reset_new_symbol();

  _pf.TEXT();
  _pf.ALIGN();

  _inFunctionArgs = true;
  _currentFunc = node->identifier();
  _offset = 8;
  if (node->arguments()) {
    _symtab.push();
    if (_function && _to_declare.find(_function->name()) != _to_declare.end()) {
      if (_function->arg_size() != node->arguments()->size()) {
        std::cerr << "number of arguments in definition doesn't match number "
                     "of arguments in previous declaration"
                  << std::endl;
      }

      for (size_t i = 0; i < _function->arg_size(); i++) {
        if (!dynamic_cast<cdk::typed_node *>(node->arguments()->node(i))
                 ->is_typed(_function->arg_type(i)->name())) {
          std::cerr << "arguments type do not match function definition"
                    << std::endl;
        }
      }
    }

    node->arguments()->accept(this, lvl);
  }
  _inFunctionArgs = false;

  if (_function->qualifier() == tPUBLIC) {
    _pf.GLOBAL(_function->name(), _pf.FUNC());
  }

  _pf.LABEL(_function->name());

  local_size_calc enter(_compiler, _symtab, _function);
  node->accept(&enter, lvl);
  _offset = -enter.call();

  if (_function->is_typed(cdk::TYPE_STRUCT) && node->arguments()) {
    for (size_t i = 0; i < _function->arg_size(); i++) {
      _function->arg(i)->set_offset(_function->arg(i)->offset() + 4);
    }
  }

  _pf.ENTER(enter.size());

  _inFunctionBody = true;
  node->block()->accept(this, lvl);
  _inFunctionBody = false;
  _currentFunc.clear();

  _symtab.pop();

  if (_function->is_typed(cdk::TYPE_VOID)) {
    _pf.LEAVE();
    _pf.RET();
  }

  _to_declare.erase(_function->name());
}

void og::postfix_writer::do_function_call_node(
    og::function_call_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  std::shared_ptr<og::symbol> function;
  if (new_symbol()) {
    function = new_symbol();
    reset_new_symbol();
  } else {
    // Function exists for sure (type_checker does this check)
    function = _symtab.find(node->identifier());
  }

  size_t size = 0;

  if (!(function->arg_size() ==
        (node->statements() ? node->statements()->size() : 0))) {
    std::cerr << "number of arguments differs from declaration of function "
              << function->name() << std::endl;
  }

  if (node->statements()) {
    for (int i = node->statements()->size() - 1; i >= 0; i--) {
      cdk::expression_node *arg =
          dynamic_cast<cdk::expression_node *>(node->statements()->node(i));
      arg->accept(this, lvl);

      if (arg->is_typed(cdk::TYPE_INT) &&
          function->arg(i)->is_typed(cdk::TYPE_DOUBLE)) {
        _pf.I2D();
        size += 4;
      } else if (!arg->is_typed(function->arg_type(i)->name())) {
        std::cerr << "wrong type for argument of type "
                  << cdk::to_string(function->arg_type(i)) << std::endl;
      }

      size += arg->type()->size();
    }
  }

  if (function->is_typed(cdk::TYPE_STRUCT)) {
    _pf.LOCAL(-function->type()->size()); // Allocated memory before call
    size += 4;
  }

  _pf.CALL(node->identifier());

  if (size) {
    _pf.TRASH(size);
  }

  if (node->is_typed(cdk::TYPE_STRUCT)) {
    _pf.LDFVAL32();
    if (!_indexing) {
      _pf.TRASH(4);
      auto type = cdk::structured_type_cast(node->type());
      size_t offset = -type->size();
      for (int i = type->length() - 1; i >= 0; i--) {
        auto elem = type->component(i);

        _pf.LOCAL(offset);

        if (elem->name() == cdk::TYPE_INT ||
            elem->name() == cdk::TYPE_POINTER ||
            elem->name() == cdk::TYPE_STRING) {
          _pf.LDINT();
        } else if (elem->name() == cdk::TYPE_DOUBLE) {
          _pf.LDDOUBLE();
        } else {
          std::cerr << "invalid type in tuple" << std::endl;
        }

        offset += elem->size();
      }
    }
  } else if (node->is_typed(cdk::TYPE_INT) ||
             node->is_typed(cdk::TYPE_POINTER) ||
             node->is_typed(cdk::TYPE_STRING)) {
    _pf.LDFVAL32();
  } else if (node->is_typed(cdk::TYPE_DOUBLE)) {
    _pf.LDFVAL64();
  }
}

void og::postfix_writer::do_block_node(og::block_node *const node, int lvl) {
  _symtab.push();
  if (node->declarations()) {
    node->declarations()->accept(this, lvl);
  }
  if (node->instructions()) {
    node->instructions()->accept(this, lvl);
  }
  _symtab.pop();
}

void og::postfix_writer::do_continue_node(og::continue_node *const node,
                                          int lvl) {
  if (inFor()) {
    _pf.JMP(mklbl(_forIncrement.top()));
  } else {
    std::cerr << "continue outside cycle" << std::endl;
  }
}

void og::postfix_writer::do_break_node(og::break_node *const node, int lvl) {
  if (inFor()) {
    _pf.JMP(mklbl(_forEnd.top()));
  } else {
    std::cerr << "break outside cycle" << std::endl;
  }
}

void og::postfix_writer::do_return_node(og::return_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  if (!_function->is_typed(cdk::TYPE_VOID)) {
    node->value()->accept(this, lvl);
    if (_function->is_typed(cdk::TYPE_INT) ||
        _function->is_typed(cdk::TYPE_POINTER) ||
        _function->is_typed(cdk::TYPE_STRING)) {
      _pf.STFVAL32();
    } else if (_function->is_typed(cdk::TYPE_STRUCT)) {

      // populate tuple
      auto returnType = cdk::structured_type_cast(_function->type());
      auto tupleType = cdk::structured_type_cast(node->value()->type());

      size_t offset = returnType->size();
      for (int i = returnType->length() - 1; i >= 0; i--) {
        auto elem = returnType->component(i);

        offset -= returnType->component(i)->size();

        if (elem->name() == cdk::TYPE_INT) {
          _pf.LOCAL(8); // Reserved zone is always first argument
          _pf.LDINT();
          _pf.INT(offset);
          _pf.ADD();

          _pf.STINT();
        } else if (elem->name() == cdk::TYPE_POINTER ||
                   elem->name() == cdk::TYPE_STRING) {

          _pf.LOCAL(8); // Reserved zone is always first argument
          _pf.LDINT();
          _pf.INT(offset);
          _pf.ADD();

          _pf.STINT();
        } else if (elem->name() == cdk::TYPE_DOUBLE) {
          if (tupleType->component(i)->name() == cdk::TYPE_INT) {
            _pf.I2D();
          }

          _pf.LOCAL(8); // Reserved zone is always first argument
          _pf.LDINT();
          _pf.INT(offset);
          _pf.ADD();

          _pf.STDOUBLE();
        } else {
          std::cerr << "invalid type in tuple to be returned" << std::endl;
        }
      }

      _pf.LOCAL(8); // Return pointer address
      _pf.LDINT();
      _pf.STFVAL32();
    } else if (_function->is_typed(cdk::TYPE_DOUBLE)) {
      if (node->value()->is_typed(cdk::TYPE_INT))
        _pf.I2D();
      _pf.STFVAL64();
    }
  }

  _pf.LEAVE();
  _pf.RET();
}

void og::postfix_writer::do_variable_decl_node(
    og::variable_decl_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  if (_inFunctionBody) {
    _assignment++;
    if (node->initializers()) {
      // Initialized local vars
      if (node->identifiers()->size() == 1) {
        auto backSymbol = new_symbol();
        reset_new_symbol();

        node->initializers()->accept(this, lvl);

        // Case: auto a = 1,2,3;
        if (node->is_typed(cdk::TYPE_STRUCT)) {
          auto tuple = cdk::structured_type_cast(node->type());

          // Leave 1 element on the stack
          int offset = _offset;
          for (int i = tuple->length() - 1; i > 0; i--) {
            auto elem = tuple->component(i);
            offset -= elem->size();

            if (elem->name() == cdk::TYPE_INT ||
                elem->name() == cdk::TYPE_POINTER ||
                elem->name() == cdk::TYPE_STRING) {
              _pf.LOCA(offset);
            } else if (elem->name() == cdk::TYPE_DOUBLE) {
              _pf.LOCAL(offset);
              _pf.STDOUBLE();
            } else {
              std::cerr << "invalid type in declaration" << std::endl;
            }
          }
        }

        _offset -= node->initializers()->type()->size();

        auto symbol = (backSymbol ? backSymbol : new_symbol());
        if (symbol) {
          symbol->set_offset(_offset);

          if (symbol->is_typed(cdk::TYPE_INT) ||
              symbol->is_typed(cdk::TYPE_POINTER) ||
              symbol->is_typed(cdk::TYPE_STRING) ||
              symbol->is_typed(cdk::TYPE_STRUCT)) {
            _pf.LOCA(_offset);
          } else if (symbol->is_typed(cdk::TYPE_DOUBLE)) {
            if (node->initializers()->is_typed(cdk::TYPE_INT)) {
              _pf.I2D();
            }
            _pf.LOCAL(_offset);
            _pf.STDOUBLE();
          } else {
            std::cerr << "invalid type in declaration" << std::endl;
          }

          reset_new_symbol();
        }
      } else {
        // More than 1 variable declared -> tuple is mandatory
        og::tuple_node *tuple =
            dynamic_cast<og::tuple_node *>(node->initializers());
        reset_new_symbol();

        auto type = cdk::structured_type_cast(tuple->type());

        tuple->accept(this, lvl);
        for (int i = type->length() - 1; i >= 0; i--) {
          std::string &id = node->identifiers()->at(i);

          _offset -= type->component(i)->size();

          auto symbol = _symtab.find(id);
          symbol->set_offset(_offset);

          if (symbol->is_typed(cdk::TYPE_INT) ||
              symbol->is_typed(cdk::TYPE_POINTER) ||
              symbol->is_typed(cdk::TYPE_STRING)) {
            _pf.LOCA(_offset);
          } else if (symbol->is_typed(cdk::TYPE_DOUBLE)) {
            _pf.LOCAL(_offset);
            _pf.STDOUBLE();
          } else {
            std::cerr << "invalid type in declaration" << std::endl;
          }
        }
      }
      _assignment--;
    } else {
      // Uninitialized local vars
      auto symbol = new_symbol();
      _offset -= node->type()->size();
      symbol->set_offset(_offset);
    }

    reset_new_symbol();
  } else if (_inFunctionArgs) {
    // Never initialized
    auto symbol = new_symbol();
    symbol->set_offset(_offset);
    reset_new_symbol();

    auto function = _symtab.find(_currentFunc);
    function->add_arg(symbol);

    _offset += node->type()->size();
  } else {
    // Global variable
    if (node->initializers()) {
      _pf.DATA();
      _pf.ALIGN();

      if (node->identifiers()->size() == 1) {
        _pf.LABEL(node->identifiers()->at(0));
        if (node->is_typed(cdk::TYPE_DOUBLE) &&
            node->initializers()->is_typed(cdk::TYPE_INT)) {
          cdk::integer_node *init =
              dynamic_cast<cdk::integer_node *>(node->initializers());
          cdk::double_node newInit(init->lineno(), init->value());
          newInit.accept(this, lvl);
        } else {
          node->initializers()->accept(this, lvl);
        }
      } else {
        og::tuple_node *tuple =
            dynamic_cast<og::tuple_node *>(node->initializers());
        for (size_t i = 0; i < tuple->length(); i++) {
          if (node->qualifier() == tPRIVATE) {
            _pf.GLOBAL(node->identifiers()->at(i), _pf.OBJ());
          } else if (node->qualifier() == tREQUIRE) {
            _to_declare.insert(node->identifiers()->at(i));
          }
          _pf.LABEL(node->identifiers()->at(i));
          tuple->elem(i)->accept(this, lvl);
        }
      }
    } else {
      // Not initialized
      _pf.BSS();
      _pf.ALIGN();

      if (node->qualifier() == tPUBLIC) {
        _pf.GLOBAL(node->identifiers()->at(0), _pf.OBJ());
      } else if (node->qualifier() == tREQUIRE) {
        _to_declare.insert(node->identifiers()->at(0));
      }

      // Only one identifier because it is uninitialized
      _pf.LABEL(node->identifiers()->at(0));

      if (node->is_typed(cdk::TYPE_INT) || node->is_typed(cdk::TYPE_POINTER) ||
          node->is_typed(cdk::TYPE_STRING)) {
        _pf.SINT(0);
      } else if (node->is_typed(cdk::TYPE_DOUBLE)) {
        _pf.SDOUBLE(0);
      } else {
        std::cerr << "invalid type in declaration" << std::endl;
      }
    }
  }
}

void og::postfix_writer::do_null_ptr_node(og::null_ptr_node *const node,
                                          int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  if (_inFunctionBody) {
    _pf.INT(0);
  } else {
    _pf.SINT(0);
  }
}

void og::postfix_writer::do_address_node(og::address_node *const node,
                                         int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  node->lvalue()->accept(this, lvl);
}

void og::postfix_writer::do_tuple_index_node(og::tuple_index_node *const node,
                                             int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  bool backInd = _indexing;
  _indexing = true;
  node->expr()->accept(this, lvl);
  _indexing = backInd;
  int index = node->index()->value() - 1;

  if (index < 0) {
    std::cerr << "invalid index for tuple" << std::endl;
  }

  auto type = cdk::structured_type_cast(node->expr()->type());

  size_t size = 0;
  for (int i = 0; i < index; i++) {
    size += type->component(i)->size();
  }

  _pf.INT(size);
  _pf.ADD();
}

void og::postfix_writer::do_size_of_node(og::size_of_node *const node,
                                         int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  _pf.INT(node->arguments()->type()->size());
}
void og::postfix_writer::do_stack_reserve_node(
    og::stack_reserve_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  node->argument()->accept(this, lvl);

  auto type = cdk::reference_type_cast(node->type());
  _pf.INT(type->referenced()->size());
  _pf.MUL();
  _pf.ALLOC();
  _pf.SP();
}

void og::postfix_writer::do_tuple_node(og::tuple_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  for (size_t i = 0; i < node->length(); i++) {
    node->elem(i)->accept(this, lvl);
  }
}

void og::postfix_writer::do_pointer_index_node(
    og::pointer_index_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  node->ptr()->accept(this, lvl);
  node->index()->accept(this, lvl);
  auto type = cdk::reference_type_cast(node->ptr()->type());
  _pf.INT(type->referenced()->size());
  _pf.MUL();
  _pf.ADD();
}

void og::postfix_writer::do_identity_node(og::identity_node *const node,
                                          int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  node->argument()->accept(this, lvl);
}
