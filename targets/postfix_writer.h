#ifndef __OG_TARGETS_POSTFIX_WRITER_H__
#define __OG_TARGETS_POSTFIX_WRITER_H__

#include "targets/basic_ast_visitor.h"
#include "targets/symbol.h"

#include <cdk/emitters/basic_postfix_emitter.h>

#include <memory>
#include <set>
#include <sstream>
#include <stack>
#include <vector>

namespace og {

//!
//! Traverse syntax tree and generate the corresponding assembly code.
//!
class postfix_writer : public basic_ast_visitor {

  std::stack<int> _forIncrement, _forEnd;

  cdk::symbol_table<og::symbol> &_symtab;
  cdk::basic_postfix_emitter &_pf;
  std::set<std::string> _to_declare;
  std::shared_ptr<og::symbol> _function;
  bool _inFunctionArgs, _inFunctionBody, _mustEnd;
  int _lbl = 0;
  int _offset = 0;
  int _assignment = 0;
  bool _evalNode = false;
  bool _indexing = false;

  std::string _currentFunc;

public:
  postfix_writer(std::shared_ptr<cdk::compiler> compiler,
                 cdk::symbol_table<og::symbol> &symtab,
                 cdk::basic_postfix_emitter &pf)
      : basic_ast_visitor(compiler), _symtab(symtab), _pf(pf),
        _function(nullptr), _inFunctionArgs(false), _inFunctionBody(false),
        _mustEnd(false) {}

public:
  ~postfix_writer() {
    for (std::string s : _to_declare) {
      _pf.EXTERN(s);
    }

    os().flush();
  }

private:
  /** Method used to generate sequential labels. */
  inline std::string mklbl(int lbl) {
    std::ostringstream oss;
    if (lbl < 0)
      oss << ".L" << -lbl;
    else
      oss << "_L" << lbl;
    return oss.str();
  }

protected:
  bool processBinaryExpression(cdk::binary_operation_node *const node, int lvl);
  void processPointerArithmetics(cdk::binary_operation_node *const node,
                                 int lvl, bool addition);
  bool inFor() { return _forEnd.size() != 0; }

public:
  // do not edit these lines
#define __IN_VISITOR_HEADER__
#include "ast/visitor_decls.h" // automatically generated
#undef __IN_VISITOR_HEADER__
  // do not edit these lines: end
};

} // namespace og

#endif
