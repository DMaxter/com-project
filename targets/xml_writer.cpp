#include <string>
#include <iostream>
#include "targets/xml_writer.h"
#include "targets/type_checker.h"
#include "ast/all.h"  // automatically generated
#include "og_parser.tab.h"

static std::string qualifier(int qual){
    switch (qual) {
        case tPRIVATE:
            return "private";
        case tPUBLIC:
            return "public";
        case tREQUIRE:
            return "require";
        default:
            return "unknown";
    }
}

//---------------------------------------------------------------------------

void og::xml_writer::do_nil_node(cdk::nil_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    closeTag(node, lvl);
}
void og::xml_writer::do_data_node(cdk::data_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    os() << std::string(lvl, ' ') << "<" << node->label() << " size='" << node->size() << "'>"
        << node->bucket() << "</" << node->label() << ">" << std::endl;
}
void og::xml_writer::do_double_node(cdk::double_node * const node, int lvl) {
    process_literal(node, lvl);
}
void og::xml_writer::do_not_node(cdk::not_node * const node, int lvl) {
    do_unary_operation(node, lvl);
}
void og::xml_writer::do_and_node(cdk::and_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_or_node(cdk::or_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}

//---------------------------------------------------------------------------

void og::xml_writer::do_sequence_node(cdk::sequence_node * const node, int lvl) {
    os() << std::string(lvl, ' ') << "<sequence_node size='" << node->size() << "'>" << std::endl;
    for (size_t i = 0; i < node->size(); i++) {
        node->node(i)->accept(this, lvl + 2);
    }
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void og::xml_writer::do_integer_node(cdk::integer_node * const node, int lvl) {
    process_literal(node, lvl);
}

void og::xml_writer::do_string_node(cdk::string_node * const node, int lvl) {
    process_literal(node, lvl);
}

//---------------------------------------------------------------------------

void og::xml_writer::do_unary_operation(cdk::unary_operation_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    node->argument()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_neg_node(cdk::neg_node * const node, int lvl) {
    do_unary_operation(node, lvl);
}

//---------------------------------------------------------------------------

void og::xml_writer::do_binary_operation(cdk::binary_operation_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    node->left()->accept(this, lvl + 2);
    node->right()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_add_node(cdk::add_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_sub_node(cdk::sub_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_mul_node(cdk::mul_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_div_node(cdk::div_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_mod_node(cdk::mod_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_lt_node(cdk::lt_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_le_node(cdk::le_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_ge_node(cdk::ge_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_gt_node(cdk::gt_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_ne_node(cdk::ne_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}
void og::xml_writer::do_eq_node(cdk::eq_node * const node, int lvl) {
    do_binary_operation(node, lvl);
}

//---------------------------------------------------------------------------

void og::xml_writer::do_variable_node(cdk::variable_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    os() << std::string(lvl, ' ') << "<" << node->label() << ">" << node->name() << "</" << node->label() << ">" << std::endl;
}

void og::xml_writer::do_rvalue_node(cdk::rvalue_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    node->lvalue()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_assignment_node(cdk::assignment_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    node->lvalue()->accept(this, lvl + 2);
    node->rvalue()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void og::xml_writer::do_evaluation_node(og::evaluation_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    node->argument()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_write_node(og::write_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    if(node->changeLine()) {
        openTag("writeln", lvl);
        node->argument()->accept(this, lvl + 2);
        closeTag("writeln", lvl);
    } else {
        openTag(node, lvl);
        node->argument()->accept(this, lvl + 2);
        closeTag(node, lvl);
    }
}

//---------------------------------------------------------------------------

void og::xml_writer::do_input_node(og::input_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    os() << std::string(lvl, ' ') << "<input/>" << std::endl;
}

//---------------------------------------------------------------------------

void og::xml_writer::do_for_node(og::for_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    openTag("initializer", lvl + 2);
    node->initialize()->accept(this, lvl + 4);
    closeTag("initializer", lvl + 2);
    openTag("condition", lvl + 2);
    node->condition()->accept(this, lvl + 4);
    closeTag("condition", lvl + 2);
    openTag("incrementer", lvl + 2);
    node->increment()->accept(this, lvl + 4);
    closeTag("incrementer", lvl + 2);
    openTag("intruction", lvl + 2);
    node->instruction()->accept(this, lvl + 4);
    closeTag("instruction", lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void og::xml_writer::do_if_node(og::if_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    openTag("condition", lvl + 2);
    node->condition()->accept(this, lvl + 4);
    closeTag("condition", lvl + 2);
    openTag("then", lvl + 2);
    node->block()->accept(this, lvl + 4);
    closeTag("then", lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_if_else_node(og::if_else_node * const node, int lvl) {
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    openTag("condition", lvl + 2);
    node->condition()->accept(this, lvl + 4);
    closeTag("condition", lvl + 2);
    openTag("then", lvl + 2);
    node->thenblock()->accept(this, lvl + 4);
    closeTag("then", lvl + 2);
    openTag("else", lvl + 2);
    node->elseblock()->accept(this, lvl + 4);
    closeTag("else", lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_function_decl_node(og::function_decl_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    os() << std::string(lvl, ' ') << "<" << node->label() << " name='" << node->identifier()
        << "' qualifier='" << qualifier(node->qualifier()) << "' type='" << cdk::to_string(node->type()) << "'>" << std::endl;
    openTag("arguments", lvl + 2);
    if (node->arguments()) {
        node->arguments()->accept(this, lvl + 4);
    }
    closeTag("arguments", lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_function_def_node(og::function_def_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    os() << std::string(lvl, ' ') << "<" << node->label() << " name='" << node->identifier()
        << "' qualifier='" << qualifier(node->qualifier()) << "' type='" << cdk::to_string(node->type()) << "'>" << std::endl;
    openTag("arguments", lvl + 2);
    if (node->arguments()) {
        node->arguments()->accept(this, lvl + 4);
    }
    closeTag("arguments", lvl + 2);
    node->block()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_function_call_node(og::function_call_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    os() << std::string(lvl, ' ') << "<" << node->label() << " name='" << node->identifier() << "'>" << std::endl;
    openTag("arguments", lvl + 2);
    if (node->statements()) {
        node->statements()->accept(this, lvl + 4);
    }
    closeTag("arguments", lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_block_node(og::block_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    openTag("declarations", lvl + 2);
    if (node->declarations()) {
        node->declarations()->accept(this, lvl + 4);
    }
    closeTag("declarations", lvl + 2);
    openTag("instructions", lvl + 2);
    if (node->instructions()) {
        node->instructions()->accept(this, lvl + 4);
    }
    closeTag("instructions", lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_continue_node(og::continue_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    closeTag(node, lvl);
}

void og::xml_writer::do_break_node(og::break_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    closeTag(node, lvl);
}

void og::xml_writer::do_return_node(og::return_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    if (node->value()) {
        node->value()->accept(this, lvl + 2);
    }
    closeTag(node, lvl);
}

void og::xml_writer::do_variable_decl_node(og::variable_decl_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    os() << std::string(lvl, ' ') << "<" << node->label() << " qualifier='" << qualifier(node->qualifier())
        << "' type='" << cdk::to_string(node->type()) << "'>" << std::endl;
    openTag("identifiers", lvl + 2);
    for(size_t i = 0; i < node->identifiers()->size(); i++){
        os() << std::string(lvl + 4, ' ') << "<identifier>" << node->identifiers()->at(i) << "</identifier>" << std::endl;
    }
    closeTag("identifiers", lvl + 2);
    openTag("initializers", lvl + 2);
    if (node->initializers()) {
        node->initializers()->accept(this, lvl + 4);
    }
    closeTag("initializers", lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_null_ptr_node(og::null_ptr_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    closeTag(node, lvl);
}

void og::xml_writer::do_address_node(og::address_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    node->lvalue()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_tuple_index_node(og::tuple_index_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node->label(), lvl);
    openTag("base", lvl + 2);
    node->expr()->accept(this, lvl + 4);
    closeTag("base", lvl + 2);
    openTag("index", lvl + 2);
    node->index()->accept(this, lvl + 4);
    closeTag("index", lvl + 2);
    closeTag(node->label(), lvl);
}

void og::xml_writer::do_size_of_node(og::size_of_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    node->arguments()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_stack_reserve_node(og::stack_reserve_node * const node, int lvl){
    openTag(node, lvl);
    node->argument()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_tuple_node(og::tuple_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    node->elems()->accept(this, lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_pointer_index_node(og::pointer_index_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    openTag("base", lvl + 2);
    node->ptr()->accept(this, lvl + 4);
    closeTag("base", lvl + 2);
    openTag("index", lvl + 2);
    node->index()->accept(this, lvl + 4);
    closeTag("index", lvl + 2);
    closeTag(node, lvl);
}

void og::xml_writer::do_identity_node(og::identity_node * const node, int lvl){
    //ASSERT_SAFE_EXPRESSIONS;
    openTag(node, lvl);
    node->argument()->accept(this, lvl + 2);
    closeTag(node, lvl);
}
