#ifndef __OG_TARGET_LOCAL_SIZE_CALC_H__
#define __OG_TARGET_LOCAL_SIZE_CALC_H__

#include "targets/basic_ast_visitor.h"

#include "targets/symbol.h"
#include <cdk/symbol_table.h>

namespace og {
class local_size_calc : public basic_ast_visitor {
private:
  cdk::symbol_table<og::symbol> &_symtab;
  std::shared_ptr<og::symbol> _function;
  size_t _local, _call;
  bool _inArgs, _mustEnd;

public:
  local_size_calc(std::shared_ptr<cdk::compiler> compiler,
                  cdk::symbol_table<og::symbol> &symtab,
                  std::shared_ptr<og::symbol> function)
      : basic_ast_visitor(compiler), _symtab(symtab), _function(function),
        _local(0), _call(0), _inArgs(false), _mustEnd(false) {}

  ~local_size_calc();

  size_t size() { return _call + _local; }

  size_t call() { return _call; }

#define __IN_VISITOR_HEADER__
#include "ast/visitor_decls.h"
#undef __IN_VISITOR_HEADER__
};
} // namespace og
#endif
