#ifndef __OG_TARGETS_SYMBOL_H__
#define __OG_TARGETS_SYMBOL_H__

#include <cdk/types/basic_type.h>
#include <memory>
#include <string>
#include <vector>

namespace og {

class symbol {
  std::shared_ptr<cdk::basic_type> _type;
  std::string _name;
  int _qualifier;
  int _offset = 0;
  bool _function;
    std::vector<std::shared_ptr<og::symbol>> _args;

public:
  symbol(const std::string &name, std::shared_ptr<cdk::basic_type> type,
         int qualifier, bool function)
      : _type(type), _name(name), _qualifier(qualifier),
        _function(function) {}

  virtual ~symbol() {
    // EMPTY
  }

  std::shared_ptr<cdk::basic_type> type() const { return _type; }

  void type(std::shared_ptr<cdk::basic_type> type) { _type = type; }

  bool is_typed(cdk::typename_type name) const { return _type->name() == name; }

  const std::string &name() const { return _name; }

  int offset() const { return _offset; }

  void set_offset(int offset) { _offset = offset; }

  bool is_function() const { return _function; }

  bool is_variable() const { return !_function; }

  bool is_global_variable() const { return is_variable() && _offset == 0; }

  bool is_local_variable() const { return is_variable() && _offset < 0; }

  bool is_argument() const { return is_variable() && _offset > 0; }

  int qualifier() const { return _qualifier; }

  void add_arg(std::shared_ptr<og::symbol> type) { _args.push_back(type); }

  size_t arg_size() { return _args.size(); }

  std::shared_ptr<og::symbol> arg(size_t i) { return _args.at(i); }

    std::shared_ptr<cdk::basic_type> arg_type(size_t i) { return _args.at(i)->type(); }
};

inline auto make_symbol(const std::string &name,
                        std::shared_ptr<cdk::basic_type> type, int qualifier,
                        bool function) {
  return std::make_shared<symbol>(name, type, qualifier, function);
}

} // namespace og

#endif
