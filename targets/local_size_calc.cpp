#include "targets/local_size_calc.h"
#include "ast/all.h"
#include "targets/symbol.h"
#include "targets/type_checker.h"
#include <cstddef>

og::local_size_calc::~local_size_calc() { os().flush(); }

void og::local_size_calc::do_add_node(cdk::add_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_and_node(cdk::and_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_assignment_node(cdk::assignment_node *const node,
                                             int lvl) {
  node->lvalue()->accept(this, lvl);
  node->rvalue()->accept(this, lvl);
}

void og::local_size_calc::do_data_node(cdk::data_node *const node, int lvl) {
  // EMPTY
}

void og::local_size_calc::do_div_node(cdk::div_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_double_node(cdk::double_node *const node,
                                         int lvl) {
  // EMPTY
}

void og::local_size_calc::do_eq_node(cdk::eq_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_ge_node(cdk::ge_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_gt_node(cdk::gt_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_integer_node(cdk::integer_node *const node,
                                          int lvl) {
  // EMPTY
}

void og::local_size_calc::do_le_node(cdk::le_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_lt_node(cdk::lt_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_mod_node(cdk::mod_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_mul_node(cdk::mul_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_neg_node(cdk::neg_node *const node, int lvl) {
  node->argument()->accept(this, lvl);
}

void og::local_size_calc::do_ne_node(cdk::ne_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_nil_node(cdk::nil_node *const node, int lvl) {
  // EMPTY
}

void og::local_size_calc::do_not_node(cdk::not_node *const node, int lvl) {
  node->argument()->accept(this, lvl);
}

void og::local_size_calc::do_or_node(cdk::or_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_rvalue_node(cdk::rvalue_node *const node,
                                         int lvl) {
  node->lvalue()->accept(this, lvl);
}

void og::local_size_calc::do_sequence_node(cdk::sequence_node *const node,
                                           int lvl) {
  size_t maxArr = node->size() - 1;
  for (size_t i = 0; i < node->size(); i++) {
    node->node(i)->accept(this, lvl);
    if (_mustEnd && i < maxArr) {
      throw std::string("unreachable code starting at line ",
                        node->node(i)->lineno());
    }
  }
  _mustEnd = false;
}

void og::local_size_calc::do_string_node(cdk::string_node *const node,
                                         int lvl) {
  // EMPTY
}

void og::local_size_calc::do_sub_node(cdk::sub_node *const node, int lvl) {
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);
}

void og::local_size_calc::do_variable_node(cdk::variable_node *const node,
                                           int lvl) {
  // EMPTY
}

void og::local_size_calc::do_address_node(og::address_node *const node,
                                          int lvl) {
  // EMPTY
}

void og::local_size_calc::do_block_node(og::block_node *const node, int lvl) {
  _symtab.push();
  if (node->declarations()) {
    node->declarations()->accept(this, lvl);
  }

  if (node->instructions()) {
    node->instructions()->accept(this, lvl);
  }
  _symtab.pop();
}

void og::local_size_calc::do_break_node(og::break_node *const node, int lvl) {
  _mustEnd = true;
}

void og::local_size_calc::do_continue_node(og::continue_node *const node,
                                           int lvl) {
  _mustEnd = true;
}

void og::local_size_calc::do_evaluation_node(og::evaluation_node *const node,
                                             int lvl) {
  node->argument()->accept(this, lvl);
}

void og::local_size_calc::do_for_node(og::for_node *const node, int lvl) {
  _symtab.push();
  node->initialize()->accept(this, lvl);
  node->instruction()->accept(this, lvl);
  node->increment()->accept(this, lvl);
  _symtab.pop();
}

void og::local_size_calc::do_function_call_node(
    og::function_call_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  if (node->is_typed(cdk::TYPE_STRUCT) && node->type()->size() > _call) {
    _call = node->type()->size();
  }
}

void og::local_size_calc::do_function_decl_node(
    og::function_decl_node *const node, int lvl) {
  // EMPTY
}

void og::local_size_calc::do_function_def_node(
    og::function_def_node *const node, int lvl) {
  _symtab.push();

  _inArgs = true;
  if (node->arguments()) {
    node->arguments()->accept(this, lvl);
  }
  _inArgs = false;

  node->block()->accept(this, lvl);
  _symtab.pop();
}

void og::local_size_calc::do_identity_node(og::identity_node *const node,
                                           int lvl) {
  node->argument()->accept(this, lvl);
}

void og::local_size_calc::do_if_else_node(og::if_else_node *const node,
                                          int lvl) {
  node->thenblock()->accept(this, lvl);
  node->elseblock()->accept(this, lvl);
}

void og::local_size_calc::do_if_node(og::if_node *const node, int lvl) {
  node->block()->accept(this, lvl);
}

void og::local_size_calc::do_input_node(og::input_node *const node, int lvl) {
  // EMPTY
}

void og::local_size_calc::do_null_ptr_node(og::null_ptr_node *const node,
                                           int lvl) {
  // EMPTY
}

void og::local_size_calc::do_pointer_index_node(
    og::pointer_index_node *const node, int lvl) {
  // EMPTY
}

void og::local_size_calc::do_return_node(og::return_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  _mustEnd = true;
}

void og::local_size_calc::do_size_of_node(og::size_of_node *const node,
                                          int lvl) {
  node->arguments()->accept(this, lvl);
}

void og::local_size_calc::do_stack_reserve_node(
    og::stack_reserve_node *const node, int lvl) {
  node->argument()->accept(this, lvl);
}

void og::local_size_calc::do_tuple_index_node(og::tuple_index_node *const node,
                                              int lvl) {
  node->expr()->accept(this, lvl);
}

void og::local_size_calc::do_tuple_node(og::tuple_node *const node, int lvl) {
  for (size_t i = 0; i < node->length(); i++) {
    node->elem(i)->accept(this, lvl);
  }
}

void og::local_size_calc::do_variable_decl_node(
    og::variable_decl_node *const node, int lvl) {
  ASSERT_SAFE_EXPRESSIONS;

  if (node->initializers()) {
    node->initializers()->accept(this, lvl);
  }

  if (!_inArgs) {
    _local += node->type()->size();
  }
}

void og::local_size_calc::do_write_node(og::write_node *const node, int lvl) {
  node->argument()->accept(this, lvl);
}
