#include "targets/type_checker.h"
#include "ast/all.h" // automatically generated
#include "og_parser.tab.h"
#include <cdk/ast/expression_node.h>
#include <cdk/types/basic_type.h>
#include <cdk/types/primitive_type.h>
#include <cdk/types/reference_type.h>
#include <cdk/types/typename_type.h>
#include <cdk/types/types.h>
#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#define ASSERT_UNSPEC                                                          \
  {                                                                            \
    if (node->type() != nullptr && !node->is_typed(cdk::TYPE_UNSPEC))          \
      return;                                                                  \
  }

//---------------------------------------------------------------------------

void og::type_checker::do_sequence_node(cdk::sequence_node *const node,
                                        int lvl) {
  for (size_t i = 0; i < node->size(); i++) {
    node->node(i)->accept(this, lvl);
  }
}

//---------------------------------------------------------------------------

void og::type_checker::do_nil_node(cdk::nil_node *const node, int lvl) {
  // EMPTY
}
void og::type_checker::do_data_node(cdk::data_node *const node, int lvl) {
  // EMPTY
}
void og::type_checker::do_double_node(cdk::double_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->type(cdk::make_primitive_type(8, cdk::TYPE_DOUBLE));
}
void og::type_checker::do_not_node(cdk::not_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->argument()->accept(this, lvl + 2);

  if (node->argument()->is_typed(cdk::TYPE_INT)) {
    node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
  } else if (node->argument()->is_typed(cdk::TYPE_UNSPEC)) {
    node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
    node->argument()->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
  } else {
    throw std::string("invalid type for unary operation");
  }
}

//---------------------------------------------------------------------------------------

void og::type_checker::processBooleanExpression(
    cdk::binary_operation_node *const node, int lvl) {
  ASSERT_UNSPEC;

  node->left()->accept(this, lvl + 2);
  node->right()->accept(this, lvl + 2);

  if (!node->left()->is_typed(cdk::TYPE_INT) ||
      !node->right()->is_typed(cdk::TYPE_INT)) {
    throw std::string("only integers are allowed in binary boolean expression");
  }

  node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
}

void og::type_checker::do_and_node(cdk::and_node *const node, int lvl) {
  processBooleanExpression(node, lvl);
}
void og::type_checker::do_or_node(cdk::or_node *const node, int lvl) {
  processBooleanExpression(node, lvl);
}

//---------------------------------------------------------------------------

void og::type_checker::do_integer_node(cdk::integer_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
}

void og::type_checker::do_string_node(cdk::string_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->type(cdk::make_primitive_type(4, cdk::TYPE_STRING));
}

//---------------------------------------------------------------------------

void og::type_checker::processUnaryExpression(
    cdk::unary_operation_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->argument()->accept(this, lvl + 2);

  if (node->argument()->is_typed(cdk::TYPE_INT)) {
    node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
  } else if (node->argument()->is_typed(cdk::TYPE_DOUBLE)) {
    node->type(cdk::make_primitive_type(8, cdk::TYPE_DOUBLE));
  } else {
    throw std::string("wrong type in argument of unary expression");
  }
}

void og::type_checker::do_neg_node(cdk::neg_node *const node, int lvl) {
  processUnaryExpression(node, lvl);
}

//---------------------------------------------------------------------------

void og::type_checker::processMultiplicativeExpression(
    cdk::binary_operation_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->left()->accept(this, lvl + 2);
  node->right()->accept(this, lvl + 2);

  if (node->left()->is_typed(cdk::TYPE_INT)) {
    if (node->right()->is_typed(cdk::TYPE_INT))
      node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
    else if (node->right()->is_typed(cdk::TYPE_DOUBLE))
      node->type(cdk::make_primitive_type(8, cdk::TYPE_DOUBLE));
    else
      throw std::string("wrong type in right argument of binary expression");
  } else if (node->left()->is_typed(cdk::TYPE_DOUBLE)) {
    if (node->right()->is_typed(cdk::TYPE_INT) ||
        node->right()->is_typed(cdk::TYPE_DOUBLE))
      node->type(cdk::make_primitive_type(8, cdk::TYPE_DOUBLE));
    else
      throw std::string("wrong type in right argument of binary expression");
  } else
    throw std::string("wrong type in left argument of binary expression");
}

void og::type_checker::processAdditiveExpression(
    cdk::binary_operation_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->left()->accept(this, lvl);
  node->right()->accept(this, lvl);

  if (node->left()->is_typed(cdk::TYPE_INT)) {
    if (node->right()->is_typed(cdk::TYPE_INT)) {
      node->type(node->left()->type());
    } else if (node->right()->is_typed(cdk::TYPE_DOUBLE)) {
      node->type(node->right()->type());
    } else if (node->right()->is_typed(cdk::TYPE_POINTER)) {
      node->type(node->right()->type());
    } else {
      throw std::string("wrong type in right argument of binary expression");
    }
  } else if (node->left()->is_typed(cdk::TYPE_DOUBLE)) {
    if (node->right()->is_typed(cdk::TYPE_DOUBLE) ||
        node->right()->is_typed(cdk::TYPE_INT)) {
      node->type(node->left()->type());
    } else {
      throw std::string("wrong type in right argument of binary expression");
    }
  } else if (node->left()->is_typed(cdk::TYPE_POINTER)) {
    if (node->right()->is_typed(cdk::TYPE_INT)) {
      node->type(node->left()->type());
    } else {
      throw std::string("wrong type in right argument of binary expression");
    }
  } else {
    throw std::string("wrong type in left argument of binary expression");
  }
}

void og::type_checker::processComparativeExpression(
    cdk::binary_operation_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->left()->accept(this, lvl + 2);
  node->right()->accept(this, lvl + 2);

  if (node->left()->is_typed(cdk::TYPE_INT) ||
      node->left()->is_typed(cdk::TYPE_DOUBLE)) {
    if (node->right()->is_typed(cdk::TYPE_INT) ||
        node->right()->is_typed(cdk::TYPE_DOUBLE))
      node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
    else
      throw std::string("wrong type in right argument of binary expression");
  } else
    throw std::string("wrong type in left argument of binary expression");
}

void og::type_checker::processEqualityExpression(
    cdk::binary_operation_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->left()->accept(this, lvl + 2);
  node->right()->accept(this, lvl + 2);

  if (node->left()->is_typed(cdk::TYPE_INT)) {
    if (node->right()->is_typed(cdk::TYPE_INT) ||
        node->right()->is_typed(cdk::TYPE_DOUBLE) ||
        node->right()->is_typed(cdk::TYPE_POINTER))
      node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
    else
      throw std::string("wrong type in right argument of binary expression");
  } else if (node->left()->is_typed(cdk::TYPE_DOUBLE)) {
    if (node->right()->is_typed(cdk::TYPE_DOUBLE) ||
        node->right()->is_typed(cdk::TYPE_INT))
      node->type(node->left()->type());
    else
      throw std::string("wrong type in right argument of binary expression");
  } else if (node->left()->is_typed(cdk::TYPE_POINTER)) {
    if (node->right()->is_typed(cdk::TYPE_POINTER) ||
        node->right()->is_typed(cdk::TYPE_INT))
      node->type(node->left()->type());
    else
      throw std::string("wrong type in right argument of binary expression");
  } else {
    throw std::string("wrong type in left argument of binary expression");
  }
}

void og::type_checker::do_add_node(cdk::add_node *const node, int lvl) {
  processAdditiveExpression(node, lvl);
}
void og::type_checker::do_sub_node(cdk::sub_node *const node, int lvl) {
  processAdditiveExpression(node, lvl);
}
void og::type_checker::do_mul_node(cdk::mul_node *const node, int lvl) {
  processMultiplicativeExpression(node, lvl);
}
void og::type_checker::do_div_node(cdk::div_node *const node, int lvl) {
  processMultiplicativeExpression(node, lvl);
}
void og::type_checker::do_mod_node(cdk::mod_node *const node, int lvl) {
  ASSERT_UNSPEC;
  node->left()->accept(this, lvl + 2);
  if (!node->left()->is_typed(cdk::TYPE_INT))
    throw std::string("wrong type in left argument of binary expression");

  node->right()->accept(this, lvl + 2);
  if (!node->right()->is_typed(cdk::TYPE_INT))
    throw std::string("wrong type in right argument of binary expression");

  node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
}
void og::type_checker::do_lt_node(cdk::lt_node *const node, int lvl) {
  processComparativeExpression(node, lvl);
}
void og::type_checker::do_le_node(cdk::le_node *const node, int lvl) {
  processComparativeExpression(node, lvl);
}
void og::type_checker::do_ge_node(cdk::ge_node *const node, int lvl) {
  processComparativeExpression(node, lvl);
}
void og::type_checker::do_gt_node(cdk::gt_node *const node, int lvl) {
  processComparativeExpression(node, lvl);
}
void og::type_checker::do_ne_node(cdk::ne_node *const node, int lvl) {
  processEqualityExpression(node, lvl);
}
void og::type_checker::do_eq_node(cdk::eq_node *const node, int lvl) {
  processEqualityExpression(node, lvl);
}

//---------------------------------------------------------------------------

void og::type_checker::do_variable_node(cdk::variable_node *const node,
                                        int lvl) {
  ASSERT_UNSPEC;
  const std::string &id = node->name();
  std::shared_ptr<og::symbol> symbol = _symtab.find(id);

  if (symbol != nullptr) {
    node->type(symbol->type());
  } else {
    throw std::string(id + " undeclared");
  }
}

void og::type_checker::do_rvalue_node(cdk::rvalue_node *const node, int lvl) {
  ASSERT_UNSPEC;

  node->lvalue()->accept(this, lvl);
  node->type(node->lvalue()->type());
}

void og::type_checker::do_assignment_node(cdk::assignment_node *const node,
                                          int lvl) {
  ASSERT_UNSPEC;

  node->lvalue()->accept(this, lvl);
  node->rvalue()->accept(this, lvl);

  if (node->lvalue()->is_typed(cdk::TYPE_POINTER)) {
    if (!node->rvalue()->is_typed(cdk::TYPE_POINTER)) {
      throw std::string("invalid type in assignment");
    }

    auto lvalue = cdk::reference_type_cast(node->lvalue()->type());
    auto rvalue = cdk::reference_type_cast(node->rvalue()->type());

    if (lvalue->referenced()->name() != rvalue->referenced()->name() &&
        rvalue->referenced()->name() != cdk::TYPE_UNSPEC) {
      throw std::string("invalid type in assignment");
    }

    node->rvalue()->type(node->lvalue()->type());
  } else if (node->lvalue()->is_typed(cdk::TYPE_STRUCT) ||
             node->rvalue()->is_typed(cdk::TYPE_STRUCT)) {
    throw std::string("invalid type in assignment");
  } else if (!(node->lvalue()->is_typed(node->rvalue()->type()->name()) ||
               (node->lvalue()->is_typed(cdk::TYPE_DOUBLE) &&
                node->rvalue()->is_typed(cdk::TYPE_INT)) ||
               ((node->rvalue()->is_typed(cdk::TYPE_POINTER) &&
                 node->lvalue()->is_typed(cdk::TYPE_INT)) &&
                (cdk::reference_type_cast(node->rvalue()->type())
                         ->referenced()
                         ->name() == cdk::TYPE_VOID ||
                 cdk::reference_type_cast(node->rvalue()->type())
                         ->referenced()
                         ->name() == cdk::TYPE_UNSPEC)))) {
    throw std::string("invalid type in assignment");
  }

  node->type(node->lvalue()->type());
}

//---------------------------------------------------------------------------

void og::type_checker::do_evaluation_node(og::evaluation_node *const node,
                                          int lvl) {
  node->argument()->accept(this, lvl + 2);
}

void og::type_checker::do_write_node(og::write_node *const node, int lvl) {
  node->argument()->accept(this, lvl + 2);
}

//---------------------------------------------------------------------------

void og::type_checker::do_input_node(og::input_node *const node, int lvl) {
  ASSERT_UNSPEC;

  node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
}

//---------------------------------------------------------------------------

void og::type_checker::do_for_node(og::for_node *const node, int lvl) {
  node->initialize()->accept(this, lvl);
  node->condition()->accept(this, lvl);
  node->increment()->accept(this, lvl);
}

//---------------------------------------------------------------------------

void og::type_checker::do_if_node(og::if_node *const node, int lvl) {
  node->condition()->accept(this, lvl + 4);
}

void og::type_checker::do_if_else_node(og::if_else_node *const node, int lvl) {
  node->condition()->accept(this, lvl + 4);
}

void og::type_checker::do_function_decl_node(og::function_decl_node *const node,
                                             int lvl) {
  std::string id;

  if (node->identifier() == "og") {
    id = "_main";
  } else if (node->identifier() == "_main") {
    id = "og";
  } else {
    id = node->identifier();
  }

  auto symbol = og::make_symbol(id, node->type(), node->qualifier(), true);

  auto previous = _symtab.find(id);
  if (previous) {
    if ((previous->is_typed(symbol->type()->name()) ||
         previous->is_typed(cdk::TYPE_UNSPEC)) &&
        previous->qualifier() == symbol->qualifier() &&
        previous->is_function()) {
      _symtab.replace(id, symbol);
      _parent->set_new_symbol(symbol);
    } else {
      throw std::string("conflicting definition for function " +
                        node->identifier());
    }
  } else {
    _symtab.insert(id, symbol);
    _parent->set_new_symbol(symbol);
  }
}

void og::type_checker::do_function_def_node(og::function_def_node *const node,
                                            int lvl) {
  std::string id;

  if (node->identifier() == "og") {
    id = "_main";
  } else if (node->identifier() == "_main") {
    id = "og";
  } else {
    id = node->identifier();
  }

  auto symbol = og::make_symbol(id, node->type(), node->qualifier(), true);

  // Check if declaration matches definition
  auto previous = _symtab.find(id);
  if (previous) {
    if (previous->is_typed(symbol->type()->name()) &&
        previous->qualifier() == symbol->qualifier() &&
        previous->is_function()) {
      previous->type(symbol->type());

      symbol = previous;
    } else {
      throw std::string("conflicting definition for function " +
                        node->identifier());
    }
  } else {
    _symtab.insert(id, symbol);
  }

  _parent->set_new_symbol(symbol);
}

void og::type_checker::do_function_call_node(og::function_call_node *const node,
                                             int lvl) {
  ASSERT_UNSPEC;

  const std::string &id = node->identifier();
  std::shared_ptr<og::symbol> symbol = _symtab.find(id);

  if (!symbol) {
    throw std::string(id + " is undeclared");
  }

  if (symbol->is_variable()) {
    throw std::string(id + " is not a function");
  }

  _parent->set_new_symbol(symbol);

  node->type(symbol->type());
}

void og::type_checker::do_block_node(og::block_node *const node, int lvl) {
  // EMPTY
}

void og::type_checker::do_continue_node(og::continue_node *const node,
                                        int lvl) {
  // EMPTY
}

void og::type_checker::do_break_node(og::break_node *const node, int lvl) {
  // EMPTY
}

void og::type_checker::do_return_node(og::return_node *const node, int lvl) {
  ASSERT_UNSPEC;

  if (!_function) {
    throw std::string("return outside function");
  }

  og::tuple_node *tuple = dynamic_cast<og::tuple_node *>(node->value());
  if (tuple) {
    tuple->accept(this, lvl + 2);
    if (_function->is_typed(cdk::TYPE_VOID)) {
      throw std::string("returning value inside procedure");
    } else if (_function->is_typed(cdk::TYPE_STRUCT) ||
               _function->is_typed(cdk::TYPE_UNSPEC)) {

      // Check function return and this return
      if (_function->is_typed(cdk::TYPE_UNSPEC)) {
        node->type(tuple->type());
        _function->type(node->type());
      } else {
        std::vector<std::shared_ptr<cdk::basic_type>> types;
        bool changed = false;
        auto funcType = cdk::structured_type_cast(_function->type());
        auto tupleType = cdk::structured_type_cast(tuple->type());
        for (size_t i = 0; i < tuple->length(); i++) {
          if (funcType->component(i)->name() ==
              tupleType->component(i)->name()) {
            types.push_back(tupleType->component(i));
            continue;
          } else if (funcType->component(i)->name() == cdk::TYPE_DOUBLE &&
                     tupleType->component(i)->name() == cdk::TYPE_INT) {
            types.push_back(funcType->component(i));
          } else if (funcType->component(i)->name() == cdk::TYPE_INT &&
                     tupleType->component(i)->name() == cdk::TYPE_DOUBLE) {
            changed = true;
            types.push_back(tupleType->component(i));
          } else {
            throw std::string("invalid return");
          }
        }

        // Check with all other returns if changed
        if (changed) {
          _function->type(cdk::make_structured_type(types));
        }
      }
    } else if (tuple->length() != 1) {
      throw std::string("returning tuple while function expects " +
                        cdk::to_string(_function->type()));
    } else {
      node->type(tuple->type());
    }
  } else {
    node->type(cdk::make_primitive_type(0, cdk::TYPE_VOID));
  }
}

void og::type_checker::do_variable_decl_node(og::variable_decl_node *const node,
                                             int lvl) {
  // Auto vars -> initializer is tuple
  if (node->is_typed(cdk::TYPE_UNSPEC) || node->is_typed(cdk::TYPE_STRUCT)) {
    std::shared_ptr<og::symbol> symbol;
    og::tuple_node *tuple =
        dynamic_cast<og::tuple_node *>(node->initializers());

    if (!tuple) {
      throw std::string("no initializers on 'auto' declaration");
    }

    tuple->accept(this, lvl);

    // auto a = 1, 2, ...
    if (node->identifiers()->size() == 1) {
      std::string &id = node->identifiers()->at(0);
      symbol = og::make_symbol(id, tuple->type(), node->qualifier(), false);

      if (!_symtab.insert(id, symbol)) {
        throw std::string(id + "redeclared");
      }
    } else {
      auto types = cdk::structured_type_cast(tuple->type());
      if (node->identifiers()->size() != types->length()) {
        throw std::string(
            "number of variables must be the same as number of initializers");
      }

      // Dereference tuple
      for (size_t i = 0; i < node->identifiers()->size(); i++) {
        std::string &id = node->identifiers()->at(i);

        symbol =
            og::make_symbol(id, types->component(i), node->qualifier(), false);

        if (!_symtab.insert(id, symbol)) {
          throw std::string(id + "redeclared");
        }
      }
    }

    _parent->set_new_symbol(symbol);
    node->type(node->initializers()->type());
  } else {
    // Non-auto vars
    std::string &id = node->identifiers()->at(0);

    bool voidPtr = false;
    // Make void *
    if (node->is_typed(cdk::TYPE_POINTER)) {
      auto nodeType = cdk::reference_type_cast(node->type());

      if (nodeType->referenced()->name() == cdk::TYPE_UNSPEC) {
        node->type(cdk::make_reference_type(
            4, cdk::make_primitive_type(1, cdk::TYPE_VOID)));
        voidPtr = true;
      }
    }

    if (node->initializers()) {
      // Primitive/Referenced type (others not allowed in YACC)
      node->initializers()->accept(this, lvl);

      if (node->is_typed(cdk::TYPE_POINTER)) {
        auto nodeType = cdk::reference_type_cast(node->type());
        auto initType = cdk::reference_type_cast(node->initializers()->type());

        if (!(nodeType->referenced()->name() ==
                  initType->referenced()->name() ||
              initType->referenced()->name() == cdk::TYPE_UNSPEC ||
              initType->referenced()->name() == cdk::TYPE_VOID)) {

          throw std::string("invalid initialization to " +
                            cdk::to_string(node->type()));
        }

        node->initializers()->type(node->type());
      } else if (!((node->is_typed(cdk::TYPE_DOUBLE) &&
                    node->initializers()->is_typed(cdk::TYPE_INT)) ||
                   node->is_typed(node->initializers()->type()->name()) ||
                   voidPtr)) {
        throw std::string("invalid initialization to type " +
                          cdk::to_string(node->type()));
        // Input node case
      } else if (node->is_typed(cdk::TYPE_DOUBLE) &&
                 dynamic_cast<og::input_node *>(node->initializers())) {
        node->initializers()->type(node->type());
      }
    }

    auto symbol = og::make_symbol(id, node->type(), node->qualifier(), false);

    if (!_symtab.insert(id, symbol)) {
      throw std::string(id + "redeclared");
    }

    _parent->set_new_symbol(symbol);
  }
}

void og::type_checker::do_null_ptr_node(og::null_ptr_node *const node,
                                        int lvl) {
  // EMPTY
}

void og::type_checker::do_address_node(og::address_node *const node, int lvl) {
  ASSERT_UNSPEC;

  node->lvalue()->accept(this, lvl);

  // NOT SURE
  if (node->lvalue()->is_typed(cdk::TYPE_DOUBLE)) {
    node->type(cdk::make_reference_type(8, node->lvalue()->type()));
  } else if (node->lvalue()->is_typed(cdk::TYPE_INT)) {
    node->type(cdk::make_reference_type(4, node->lvalue()->type()));
  } else if (node->lvalue()->is_typed(cdk::TYPE_POINTER)) {
    node->type(cdk::make_reference_type(4, node->lvalue()->type()));
  } else {
    throw std::string("invalid type for address operator");
  }
}

void og::type_checker::do_tuple_index_node(og::tuple_index_node *const node,
                                           int lvl) {
  ASSERT_UNSPEC;

  node->expr()->accept(this, lvl);
  node->index()->accept(this, lvl);
  auto type = cdk::structured_type_cast(node->expr()->type());

  if (!node->index()->is_typed(cdk::TYPE_INT)) {
    throw std::string("invalid type for tuple index operator");
  } else if (node->index()->value() > (int)type->length()) {
    throw std::string("invalid index for tuple of size " + type->length());
  }

  node->type(type->component(node->index()->value() - 1));
}

void og::type_checker::do_size_of_node(og::size_of_node *const node, int lvl) {
  ASSERT_UNSPEC;

  node->arguments()->accept(this, lvl);

  if (node->arguments()->is_typed(cdk::TYPE_UNSPEC)) {
    throw std::string("invalid type inside sizeof operator");
  }

  node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
}

void og::type_checker::do_stack_reserve_node(og::stack_reserve_node *const node,
                                             int lvl) {
  ASSERT_UNSPEC;

  node->argument()->accept(this, lvl);

  if (!node->argument()->is_typed(cdk::TYPE_INT)) {
    throw std::string("integer expected in stack allocation operator");
  }

  node->type(cdk::make_reference_type(
      4, cdk::make_primitive_type(0, cdk::TYPE_UNSPEC)));
}

void og::type_checker::do_tuple_node(og::tuple_node *const node, int lvl) {
  ASSERT_UNSPEC;

  if (node->length() == 1) {
    node->elem(0)->accept(this, lvl);
    node->type(dynamic_cast<cdk::expression_node *>(node->elem(0))->type());
  } else {
    std::vector<std::shared_ptr<cdk::basic_type>> types;

    for (size_t i = 0; i < node->length(); i++) {
      node->elem(i)->accept(this, lvl);
      types.push_back(
          dynamic_cast<cdk::expression_node *>(node->elem(i))->type());
    }

    node->type(cdk::make_structured_type(types));
  }
}

void og::type_checker::do_pointer_index_node(og::pointer_index_node *const node,
                                             int lvl) {
  ASSERT_UNSPEC;

  node->ptr()->accept(this, lvl);
  node->index()->accept(this, lvl);

  if (node->ptr()->is_typed(cdk::TYPE_POINTER) &&
      node->index()->is_typed(cdk::TYPE_INT)) {
    auto ptr = cdk::reference_type_cast(node->ptr()->type());
    if (ptr->referenced()->name() == cdk::TYPE_VOID ||
        ptr->referenced()->name() == cdk::TYPE_UNSPEC) {
      throw std::string("cannot index void pointer");
    }

    node->type(ptr->referenced());
  } else {
    throw std::string("invalid type(s) for pointer index operator");
  }
}

void og::type_checker::do_identity_node(og::identity_node *const node,
                                        int lvl) {
  ASSERT_UNSPEC;

  node->argument()->accept(this, lvl);

  if (node->argument()->is_typed(cdk::TYPE_INT)) {
    node->type(cdk::make_primitive_type(4, cdk::TYPE_INT));
  } else if (node->argument()->is_typed(cdk::TYPE_DOUBLE)) {
    node->type(cdk::make_primitive_type(8, cdk::TYPE_DOUBLE));
  } else {
    throw std::string("invalid type for identity operator");
  }
}
