#ifndef __OG_AST_TUPLE_INDEX_H__
#define __OG_AST_TUPLE_INDEX_H__

#include <cdk/ast/lvalue_node.h>
#include <cdk/ast/integer_node.h>
#include <cdk/ast/expression_node.h>

namespace og {
    class tuple_index_node: public cdk::lvalue_node {
    private:
        cdk::expression_node *_expr;
		cdk::integer_node *_index;

    public:
        inline tuple_index_node(int lineno, cdk::expression_node *expr, cdk::integer_node *index) :
            cdk::lvalue_node(lineno), _expr(expr), _index(index) {
        }

        inline cdk::expression_node *expr() {
            return _expr;
        }

        inline cdk::integer_node *index() {
            return _index;
        }

        void accept(basic_ast_visitor *sp, int level) {
            sp->do_tuple_index_node(this, level);
        }
    };

} // og

#endif
