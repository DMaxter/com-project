#ifndef __OG_AST_NULLPTR_H__
#define __OG_AST_NULLPTR_H__

#include <cdk/ast/expression_node.h>
#include <cdk/types/typename_type.h>
#include <cdk/types/types.h>

namespace og {
    class null_ptr_node: public cdk::expression_node {
    public:
        inline null_ptr_node(int lineno) :
            cdk::expression_node(lineno) {
            cdk::typed_node::type(cdk::make_reference_type(4, cdk::make_primitive_type(1, cdk::TYPE_VOID)));
        }

        void accept(basic_ast_visitor *sp, int level) {
            sp->do_null_ptr_node(this, level);
        }
    };

} //og

#endif
