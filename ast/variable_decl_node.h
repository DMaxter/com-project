#ifndef __OG_AST_VARIABLE_DECL_H__
#define __OG_AST_VARIABLE_DECL_H__

#include <memory>
#include <vector>
#include <cdk/ast/typed_node.h>
#include <cdk/ast/sequence_node.h>
#include <cdk/ast/expression_node.h>
#include <cdk/types/basic_type.h>

namespace og {

  class variable_decl_node: public cdk::typed_node {
  private:
    int _qualifier;
    std::vector<std::string> *_identifiers;
    cdk::expression_node *_initializers;

  public:
      inline variable_decl_node(int lineno, int qualifier, cdk::basic_type *type, std::vector<std::string> *identifiers,
                              cdk::expression_node *initializers) :
        cdk::typed_node(lineno), _qualifier(qualifier), _identifiers(identifiers), _initializers(initializers) {
        cdk::typed_node::type(std::shared_ptr<cdk::basic_type>(type));
    }

    inline int qualifier() {
      return _qualifier;
    }

      inline std::vector<std::string> *identifiers() {
      return _identifiers;
    }

    inline cdk::expression_node *initializers() {
      return _initializers;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_variable_decl_node(this, level);
    }

  };

} //og

#endif
