#ifndef __OG_AST_FUNCTION_DECL_NODE_H__
#define __OG_AST_FUNCTION_DECL_NODE_H__

#include <string>
#include <cdk/ast/typed_node.h>
#include <cdk/ast/sequence_node.h>
#include <cdk/types/basic_type.h>
#include <cdk/types/primitive_type.h>
#include <cdk/types/typename_type.h>

namespace og {

    /**
     * Class for describing function declarations
     */
    class function_decl_node: public cdk::typed_node {
    private:
        int _qualifier;
        std::string _identifier;
        cdk::sequence_node *_arguments;

    public:
        inline function_decl_node(int lineno, int qualifier, const std::string &identifier, cdk::sequence_node *arguments) :
            cdk::typed_node(lineno), _qualifier(qualifier), _identifier(identifier), _arguments(arguments) {
            cdk::typed_node::type(std::shared_ptr<cdk::basic_type>(new cdk::primitive_type(0, cdk::typename_type::TYPE_VOID)));
        }

        inline function_decl_node(int lineno, int qualifier, cdk::basic_type *type, const std::string &identifier, cdk::sequence_node *arguments) :
            cdk::typed_node(lineno), _qualifier(qualifier), _identifier(identifier), _arguments(arguments) {
            cdk::typed_node::type(std::shared_ptr<cdk::basic_type>(type));
        }

        inline int qualifier(){
            return _qualifier;
        }

        inline const std::string &identifier() {
            return _identifier;
        }

        inline cdk::sequence_node *arguments() {
            return _arguments;
        }

        void accept(basic_ast_visitor *sp, int level) {
            sp->do_function_decl_node(this, level);
        }

    };

} // og

#endif
