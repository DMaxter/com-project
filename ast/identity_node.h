#ifndef __OG_AST_IDENTITY_NODE_H___
#define __OG_AST_IDENTITY_NODE_H___

#include <cdk/ast/expression_node.h>

namespace og {

  /**
   * Class for describing identity nodes
   */
  class identity_node: public cdk::expression_node {
  private:
    cdk::expression_node *_argument;

  public:
    inline identity_node(int lineno, cdk::expression_node *argument) :
        cdk::expression_node(lineno), _argument(argument) {
    }

    inline cdk::expression_node *argument() {
        return _argument;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_identity_node(this, level);
    }
  };

} // og

#endif
