#ifndef __OG_AST_STACK_RESERVE_H__
#define __OG_AST_STACK_RESERVE_H__

#include <cdk/ast/unary_operation_node.h>

namespace og {
    class stack_reserve_node : public cdk::unary_operation_node {
    public:
        inline stack_reserve_node(int lineno, cdk::expression_node *argument) :
            unary_operation_node(lineno, argument) {
        }

        void accept(basic_ast_visitor *sp, int level){
            sp->do_stack_reserve_node(this, level);
        }
    };
} // og

#endif
