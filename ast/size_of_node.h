#ifndef __OG_AST_SIZE_OF_NODE_H__
#define __OG_AST_SIZE_OF_NODE_H__

#include <cdk/ast/expression_node.h>

namespace og {

  /**
   * Class for describing size_of nodes.
   */
  class size_of_node: public cdk::expression_node {
  private:
    cdk::expression_node *_arguments;

  public:
    inline size_of_node(int lineno, cdk::expression_node *arguments) :
        cdk::expression_node(lineno), _arguments(arguments) {
    }

    inline cdk::expression_node *arguments() {
      return _arguments;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_size_of_node(this, level);
    }

  };

} // og

#endif
