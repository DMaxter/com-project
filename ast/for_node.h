#ifndef __OG_AST_FOR_NODE_H__
#define __OG_AST_FOR_NODE_H__

#include <cdk/ast/basic_node.h>
#include <cdk/ast/sequence_node.h>

namespace og {

    /**
     * Class for describing for-cycle nodes.
     */
    class for_node: public cdk::basic_node {
    private:
        cdk::sequence_node *_initialize;
        cdk::sequence_node *_condition;
        cdk::sequence_node *_increment;
        cdk::basic_node *_instruction;

    public:
        inline for_node(int lineno, cdk::sequence_node *initialize, cdk::sequence_node *condition, cdk::sequence_node *increment, cdk::basic_node *instruction) :
            basic_node(lineno), _initialize(initialize), _condition(condition), _increment(increment), _instruction(instruction) {
        }

        inline cdk::sequence_node *initialize() {
            return _initialize;
        }

        inline cdk::sequence_node *condition() {
            return _condition;
        }

        inline cdk::sequence_node *increment() {
            return _increment;
        }

        inline cdk::basic_node *instruction() {
            return _instruction;
        }

        void accept(basic_ast_visitor *sp, int level) {
            sp->do_for_node(this, level);
        }

    };

} // og

#endif
