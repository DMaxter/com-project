#ifndef __OG_AST_FUNCTION_DEF_NODE_H__
#define __OG_AST_FUNCTION_DEF_NODE_H__

#include <string>
#include <memory>
#include <cdk/ast/sequence_node.h>
#include <cdk/ast/typed_node.h>
#include <cdk/types/basic_type.h>
#include <cdk/types/primitive_type.h>
#include <cdk/types/typename_type.h>
#include "ast/block_node.h"

namespace og {

  /**
   * Class for describing function definitions.
   */
  class function_def_node: public cdk::typed_node {
  private:
    int _qualifier;
    std::string _identifier;
    cdk::sequence_node *_arguments;
    og::block_node *_block;

  public:
    inline function_def_node(int lineno, int qualifier, const std::string &identifier, cdk::sequence_node *arguments,
                             og::block_node *block) :
        cdk::typed_node(lineno), _qualifier(qualifier), _identifier(identifier), _arguments(arguments), _block(block) {
      cdk::typed_node::type(std::shared_ptr<cdk::basic_type>(new cdk::primitive_type(0, cdk::typename_type::TYPE_VOID)));
    }

    inline function_def_node(int lineno, int qualifier, cdk::basic_type *type, const std::string &identifier,
                             cdk::sequence_node *arguments, og::block_node *block) :
        cdk::typed_node(lineno), _qualifier(qualifier), _identifier(identifier), _arguments(arguments), _block(block) {
      cdk::typed_node::type(std::shared_ptr<cdk::basic_type>(type));
    }

    inline int qualifier() {
      return _qualifier;
    }

    inline const std::string &identifier() const {
      return _identifier;
    }

    inline cdk::sequence_node *arguments() {
      return _arguments;
    }

    inline og::block_node *block() {
      return _block;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_function_def_node(this, level);
    }
  };


} // og

#endif
