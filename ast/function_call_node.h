#ifndef __OG_AST_FUNCTION_CALL_NODE_H__
#define __OG_AST_FUNCTION_CALL_NODE_H__

#include <cdk/ast/expression_node.h>
#include <cdk/ast/sequence_node.h>

namespace og {

  /**
   * Class for describing function_call nodes.
   */
  class function_call_node: public cdk::expression_node {
  private:
    std::string _identifier;
    cdk::sequence_node *_statements;

  public:
    inline function_call_node(int lineno, const std::string &identifier, cdk::sequence_node *statements) :
        cdk::expression_node(lineno), _identifier(identifier), _statements(statements) {
    }

    inline std::string &identifier() {
      return _identifier;
    }

    inline cdk::sequence_node *statements() {
      return _statements;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_function_call_node(this, level);
    }

  };

} // og

#endif
