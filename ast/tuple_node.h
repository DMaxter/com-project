#ifndef __OG_AST_TUPLE_H__
#define __OG_AST_TUPLE_H__

#include <vector>
#include <memory>
#include <cdk/ast/sequence_node.h>
#include <cdk/ast/expression_node.h>

namespace og {
    class tuple_node: public cdk::expression_node {
    private:
        cdk::sequence_node *_elems;

    public:
        inline tuple_node(int lineno, cdk::sequence_node *elems) :
            cdk::expression_node(lineno), _elems(elems) {
        }

        inline basic_node *elem(int i) {
            return _elems->node(i);
        }

        inline cdk::sequence_node *elems() {
            return _elems;
        }

        inline size_t length() const {
            return _elems->size();
        }

        void accept(basic_ast_visitor *sp, int level) {
            sp->do_tuple_node(this, level);
        }
    };

} // og

#endif
