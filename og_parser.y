%{
    //-- don't change *any* of these: if you do, you'll break the compiler.
#include <cdk/compiler.h>
#include "ast/all.h"
#define LINE               compiler->scanner()->lineno()
#define yylex()            compiler->scanner()->scan()
#define yyerror(s)         compiler->scanner()->error(s)
#define YYPARSE_PARAM_TYPE std::shared_ptr<cdk::compiler>
#define YYPARSE_PARAM      compiler
    //-- don't change *any* of these --- END!
%}

%union {
    int i;
    double d;
    std::string *s;
    og::block_node *block;
    cdk::basic_node *node;
    cdk::basic_type *type;
    cdk::lvalue_node *lvalue;
    cdk::expression_node *expr;
    cdk::sequence_node *sequence;
    std::vector<std::string> *id;
};

%token <i> tINTEGER
%token <d> tREAL
%token <s> tIDENTIFIER tSTRING
%token <expr> tNULLPTR
%token tPRIVATE tPUBLIC tREQUIRE tPROCEDURE
%token tINTEGER_TYPE tREAL_TYPE tSTRING_TYPE tAUTO tPTR
%token tSTRING tINTEGER tREAL
%token tIF tTHEN tELSE tELIF
%token tFOR tDO
%token tAND tOR
%token tGE tLE tEQ tNE
%token tSIZEOF tWRITE tWRITELN tINPUT
%token tBREAK tCONTINUE tRETURN

%type <s> string
%type <block> block
%type <node> decl var_decl func_decl func_def cond_instr iter_instr priv_var auto_var nauto_var non_init elif
%type <id> id ids
%type <type> type auto_type
%type <lvalue> lvalue
%type <expr> expr
%type <sequence> program decls exprs args instr instrs block_vars no_auto_vars opt_for_param opt_expr

%nonassoc tIFX
%nonassoc tIF tELSE tELIF

%right '='
%left tOR
%left tAND
%nonassoc '~'
%left tEQ tNE
%left tGE tLE '<' '>'
%nonassoc '@'
%left '+' '-'
%left '*' '/' '%'
%nonassoc '?'
%nonassoc '['
%left tUNARY

%{
    //-- The rules below will be included in yyparse, the main parsing function.
%}
%%

program : decls                                                                           { compiler->ast($$ = $1); }
        ;

decls :        decl                                                                       { $$ = new cdk::sequence_node(LINE, $1); }
      | decls  decl                                                                       { $$ = new cdk::sequence_node(LINE, $2, $1); }
      ;

decl : var_decl ';'                                                                       { $$ = $1; }
     | func_decl                                                                          { $$ = $1; }
     | func_def                                                                           { $$ = $1; }
     ;

non_init : type id                                                                        { $$ = new og::variable_decl_node(LINE, tPRIVATE, $1, $2, nullptr); }
         ;

priv_var : type      id '=' expr                                                          { $$ = new og::variable_decl_node(LINE, tPRIVATE, $1, $2, $4); }
         | auto_type ids         '=' exprs                                                { $$ = new og::variable_decl_node(LINE, tPRIVATE, $1, $2, new og::tuple_node(LINE, $4)); }
         | non_init                                                                       { $$ = $1; }
         ;

var_decl : tPUBLIC  type      id                                                          { $$ = new og::variable_decl_node(LINE, tPUBLIC, $2, $3, nullptr); }
         | tREQUIRE type      id                                                          { $$ = new og::variable_decl_node(LINE, tREQUIRE, $2, $3, nullptr); }
         | tPUBLIC  type      id '=' expr                                                 { $$ = new og::variable_decl_node(LINE, tPUBLIC, $2, $3, $5); }
         | tPUBLIC  auto_type ids         '=' exprs                                       { $$ = new og::variable_decl_node(LINE, tPUBLIC, $2, $3, new og::tuple_node(LINE, $5)); }
         | priv_var                                                                       { $$ = $1; }
         ;

auto_var :         auto_type ids '=' exprs                                                { $$ = new og::variable_decl_node(LINE, tPRIVATE, $1, $2, new og::tuple_node(LINE, $4)); }
         | tPUBLIC auto_type ids '=' exprs                                                { $$ = new og::variable_decl_node(LINE, tPUBLIC, $2, $3, new og::tuple_node(LINE, $5)); }
         ;

nauto_var : tPUBLIC  type id                                                     { $$ = new og::variable_decl_node(LINE, tPUBLIC, $2, $3, nullptr); }
          | tREQUIRE type id                                                     { $$ = new og::variable_decl_node(LINE, tREQUIRE, $2, $3, nullptr); }
          | tPUBLIC  type id '=' expr                                            { $$ = new og::variable_decl_node(LINE, tPUBLIC, $2, $3, $5); }
          |          type id                                                     { $$ = new og::variable_decl_node(LINE, tPRIVATE, $1, $2, nullptr); }
          |          type id '=' expr                                            { $$ = new og::variable_decl_node(LINE, tPRIVATE, $1, $2, $4); }
          ;

func_decl :            type       tIDENTIFIER '('      ')'                                { $$ = new og::function_decl_node(LINE, tPRIVATE, $1, *$2, nullptr); delete $2; }
          |            tPROCEDURE tIDENTIFIER '('      ')'                                { $$ = new og::function_decl_node(LINE, tPRIVATE, *$2, nullptr); delete $2; }
          | tPUBLIC    type       tIDENTIFIER '('      ')'                                { $$ = new og::function_decl_node(LINE, tPUBLIC, $2, *$3, nullptr); delete $3; }
          | tPUBLIC    tPROCEDURE tIDENTIFIER '('      ')'                                { $$ = new og::function_decl_node(LINE, tPUBLIC, *$3, nullptr); delete $3; }
          | tREQUIRE   type       tIDENTIFIER '('      ')'                                { $$ = new og::function_decl_node(LINE, tREQUIRE, $2, *$3, nullptr); delete $3; }
          | tREQUIRE   tPROCEDURE tIDENTIFIER '('      ')'                                { $$ = new og::function_decl_node(LINE, tREQUIRE, *$3, nullptr); delete $3; }
          |            type       tIDENTIFIER '(' args ')'                                { $$ = new og::function_decl_node(LINE, tPRIVATE, $1, *$2, $4); delete $2; }
          |            tPROCEDURE tIDENTIFIER '(' args ')'                                { $$ = new og::function_decl_node(LINE, tPRIVATE, *$2, $4); delete $2; }
          | tPUBLIC    type       tIDENTIFIER '(' args ')'                                { $$ = new og::function_decl_node(LINE, tPUBLIC, $2, *$3, $5); delete $3; }
          | tPUBLIC    tPROCEDURE tIDENTIFIER '(' args ')'                                { $$ = new og::function_decl_node(LINE, tPUBLIC, *$3, $5); delete $3; }
          | tREQUIRE   type       tIDENTIFIER '(' args ')'                                { $$ = new og::function_decl_node(LINE, tREQUIRE, $2, *$3, $5); delete $3; }
          | tREQUIRE   tPROCEDURE tIDENTIFIER '(' args ')'                                { $$ = new og::function_decl_node(LINE, tREQUIRE, *$3, $5); delete $3; }
          ;

func_def :            type       tIDENTIFIER '('      ')' block                           { $$ = new og::function_def_node(LINE, tPRIVATE, $1, *$2, nullptr, $5); delete $2; }
         | tPROCEDURE            tIDENTIFIER '('      ')' block                           { $$ = new og::function_def_node(LINE, tPRIVATE, *$2, nullptr, $5); delete $2; }
         |            auto_type  tIDENTIFIER '('      ')' block                           { $$ = new og::function_def_node(LINE, tPRIVATE, $1, *$2, nullptr, $5); delete $2; }
         | tPUBLIC    type       tIDENTIFIER '('      ')' block                           { $$ = new og::function_def_node(LINE, tPUBLIC, $2, *$3, nullptr, $6); delete $3; }
         | tPUBLIC    auto_type  tIDENTIFIER '('      ')' block                           { $$ = new og::function_def_node(LINE, tPUBLIC, $2, *$3, nullptr, $6); delete $3; }
         | tPUBLIC    tPROCEDURE tIDENTIFIER '('      ')' block                           { $$ = new og::function_def_node(LINE, tPUBLIC, *$3, nullptr, $6); delete $3; }
         |            type       tIDENTIFIER '(' args ')' block                           { $$ = new og::function_def_node(LINE, tPRIVATE, $1, *$2, $4, $6); delete $2; }
         |            auto_type  tIDENTIFIER '(' args ')' block                           { $$ = new og::function_def_node(LINE, tPRIVATE, $1, *$2, $4, $6); delete $2; }
         |            tPROCEDURE tIDENTIFIER '(' args ')' block                           { $$ = new og::function_def_node(LINE, tPRIVATE, *$2, $4, $6); delete $2; }
         | tPUBLIC    type       tIDENTIFIER '(' args ')' block                           { $$ = new og::function_def_node(LINE, tPUBLIC, $2, *$3, $5, $7); delete $3; }
         | tPUBLIC    auto_type  tIDENTIFIER '(' args ')' block                           { $$ = new og::function_def_node(LINE, tPUBLIC, $2, *$3, $5, $7); delete $3; }
         | tPUBLIC    tPROCEDURE tIDENTIFIER '(' args ')' block                           { $$ = new og::function_def_node(LINE, tPUBLIC, *$3, $5, $7); delete $3; }
         ;

type : tINTEGER_TYPE                                                                      { $$ = new cdk::primitive_type(4, cdk::typename_type::TYPE_INT); }
     | tREAL_TYPE                                                                         { $$ = new cdk::primitive_type(8, cdk::typename_type::TYPE_DOUBLE); }
     | tSTRING_TYPE                                                                       { $$ = new cdk::primitive_type(4, cdk::typename_type::TYPE_STRING); }
     | tPTR '<'   type    '>'                                                             { $$ = new cdk::reference_type(4, std::shared_ptr<cdk::basic_type>($3)); }
     | tPTR '<' auto_type '>'                                                             { $$ = new cdk::reference_type(4, std::shared_ptr<cdk::basic_type>($3)); }
     ;

auto_type : tAUTO                                                                         { $$ = new cdk::primitive_type(0, cdk::typename_type::TYPE_UNSPEC); }
          ;

exprs :           expr                                                                    { $$ = new cdk::sequence_node(LINE, $1); }
      | exprs ',' expr                                                                    { $$ = new cdk::sequence_node(LINE, $3, $1); }
      ;

id : tIDENTIFIER                                                                          { $$ = new std::vector<std::string>(); $$->push_back(*$1); delete $1; }

ids :         id                                                                          { $$ = $1; }
    | ids ',' tIDENTIFIER                                                                 { $$ = $1; $$->push_back(*$3); delete $3; }
    ;

args :          non_init                                                                  { $$ = new cdk::sequence_node(LINE, $1); }
     | args ',' non_init                                                                  { $$ = new cdk::sequence_node(LINE, $3, $1); }
     ;

block_vars :            priv_var ';'                                                      { $$ = new cdk::sequence_node(LINE, $1); }
           | block_vars priv_var ';'                                                      { $$ = new cdk::sequence_node(LINE, $2, $1); }
           ;

opt_for_param : auto_var                                                                  { $$ = new cdk::sequence_node(LINE, $1); }
              | no_auto_vars                                                              { $$ = $1; }
              | opt_expr                                                                  { $$ = $1; }
              ;

no_auto_vars :                  nauto_var                                                 { $$ = new cdk::sequence_node(LINE, $1); }
             | no_auto_vars ',' nauto_var                                                 { $$ = new cdk::sequence_node(LINE, $3, $1); }
             ;


opt_expr : /* EMPTY */                                                                    { $$ = new cdk::sequence_node(LINE); }
         | exprs                                                                          { $$ = $1; }
         ;

instrs :        instr                                                                     { $$ = $1; }
       | instrs instr                                                                     { $$ = new cdk::sequence_node(LINE, $2, $1); }
       ;

block : '{'                    '}'                                                        { $$ = new og::block_node(LINE, nullptr, new cdk::sequence_node(LINE)); }
      | '{' block_vars        '}'                                                         { $$ = new og::block_node(LINE, $2, new cdk::sequence_node(LINE)); }
      | '{'            instrs '}'                                                         { $$ = new og::block_node(LINE, nullptr, $2); }
      | '{' block_vars instrs '}'                                                         { $$ = new og::block_node(LINE, $2, $3); }
      ;


expr : tINTEGER                                                                           { $$ = new cdk::integer_node(LINE, $1); }
     | tREAL                                                                              { $$ = new cdk::double_node(LINE, $1); }
     | string                                                                             { $$ = new cdk::string_node(LINE, $1); }
     | tNULLPTR                                                                           { $$ = new og::null_ptr_node(LINE); }
     /* ARITHMETIC */
     | expr '+' expr                                                                      { $$ = new cdk::add_node(LINE, $1, $3); }
     | expr '-' expr                                                                      { $$ = new cdk::sub_node(LINE, $1, $3); }
     | expr '*' expr                                                                      { $$ = new cdk::mul_node(LINE, $1, $3); }
     | expr '/' expr                                                                      { $$ = new cdk::div_node(LINE, $1, $3); }
     | expr '%' expr                                                                      { $$ = new cdk::mod_node(LINE, $1, $3); }
     /* COMPARISONS */
     | expr '<' expr                                                                      { $$ = new cdk::lt_node(LINE, $1, $3); }
     | expr '>' expr                                                                      { $$ = new cdk::gt_node(LINE, $1, $3); }
     | expr tGE expr                                                                      { $$ = new cdk::ge_node(LINE, $1, $3); }
     | expr tLE expr                                                                      { $$ = new cdk::le_node(LINE, $1, $3); }
     | expr tNE expr                                                                      { $$ = new cdk::ne_node(LINE, $1, $3); }
     | expr tEQ expr                                                                      { $$ = new cdk::eq_node(LINE, $1, $3); }
     /* UNARY */
     | '+' expr %prec tUNARY                                                              { $$ = new og::identity_node(LINE, $2); }
     | '-' expr %prec tUNARY                                                              { $$ = new cdk::neg_node(LINE, $2); }
     | '~' expr                                                                           { $$ = new cdk::not_node(LINE, $2); }
     | '[' expr ']'                                                                       { $$ = new og::stack_reserve_node(LINE, $2); }
     /* LOGICAL */
     | expr tAND expr                                                                     { $$ = new cdk::and_node(LINE, $1, $3); }
     | expr tOR expr                                                                      { $$ = new cdk::or_node(LINE, $1, $3); }
     /* FUNCTIONS */
     | tIDENTIFIER '('       ')'                                                          { $$ = new og::function_call_node(LINE, *$1, nullptr); delete $1; }
     | tIDENTIFIER '(' exprs ')'                                                          { $$ = new og::function_call_node(LINE, *$1, $3); delete $1; }
     | tSIZEOF     '(' exprs ')'                                                          { $$ = new og::size_of_node(LINE, new og::tuple_node(LINE, $3)); }
     /* OTHER */
     | tINPUT                                                                             { $$ = new og::input_node(LINE); }
     | '(' expr ')'                                                                       { $$ = $2; }
     | lvalue                                                                             { $$ = new cdk::rvalue_node(LINE, $1); }
     | lvalue '?'                                                                         { $$ = new og::address_node(LINE, $1); }
     | lvalue '=' expr                                                                    { $$ = new cdk::assignment_node(LINE, $1, $3); }
     ;

instr : cond_instr                                                                        { $$ = new cdk::sequence_node(LINE, $1); }
      | iter_instr                                                                        { $$ = new cdk::sequence_node(LINE, $1); }
      | tWRITE exprs ';'                                                                  { $$ = new cdk::sequence_node(LINE, new og::write_node(LINE, new og::tuple_node(LINE, $2), false)); }
      | tWRITELN exprs ';'                                                                { $$ = new cdk::sequence_node(LINE, new og::write_node(LINE, new og::tuple_node(LINE, $2), true)); }
      | tRETURN ';'                                                                       { $$ = new cdk::sequence_node(LINE, new og::return_node(LINE, nullptr)); }
      | tRETURN exprs ';'                                                                 { $$ = new cdk::sequence_node(LINE, new og::return_node(LINE, new og::tuple_node(LINE, $2))); }
      | expr ';'                                                                          { $$ = new cdk::sequence_node(LINE, new og::evaluation_node(LINE, $1)); }
      | tCONTINUE                                                                         { $$ = new cdk::sequence_node(LINE, new og::continue_node(LINE)); }
      | tBREAK                                                                            { $$ = new cdk::sequence_node(LINE, new og::break_node(LINE)); }
      | block                                                                             { $$ = new cdk::sequence_node(LINE, $1); }
      ;

cond_instr : tIF expr tTHEN instr %prec tIFX                                              { $$ = new og::if_node(LINE, $2, $4); }
           | tIF expr tTHEN instr elif                                                    { $$ = new og::if_else_node(LINE, $2, $4, $5); }
           ;

elif : tELSE instr                                                                        { $$ = $2; }
     | tELIF expr tTHEN instr %prec tIFX                                                  { $$ = new og::if_node(LINE, $2, $4); }
     | tELIF expr tTHEN instr elif                                                        { $$ = new og::if_else_node(LINE, $2, $4, $5); }
     ;

iter_instr : tFOR opt_for_param ';' opt_expr ';' opt_expr tDO instr                       { $$ = new og::for_node(LINE, $2, $4, $6, $8); }
           ;

string : tSTRING                                                                          { $$ = $1; }
       | string tSTRING                                                                   { $$ = new std::string(*$1 + *$2); delete $1; delete $2; }
       ;

lvalue : tIDENTIFIER                                                                      { $$ = new cdk::variable_node(LINE, $1); }
       | expr '[' expr ']'                                                                { $$ = new og::pointer_index_node(LINE, $1, $3); }
       | expr '@' tINTEGER                                                                { $$ = new og::tuple_index_node(LINE, $1, new cdk::integer_node(LINE, $3)); }
       ;
%%
